package vip.appdesign.common.controller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Controller 通用处理层
 */
public class BaseController {
    /**
     * 日志类
     */
    protected Logger logger = LoggerFactory.getLogger(getClass());

}
