package vip.appdesign.common.redis;

import com.google.gson.Gson;
import jakarta.annotation.Resource;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.data.redis.core.ValueOperations;
import org.springframework.stereotype.Component;

import java.util.concurrent.TimeUnit;

/**
 * redis工具类
 * @author sun
 */
@Component
public class RedisUtils {
    @Resource
    private RedisTemplate<String,Object> redisTemplate;
    @Resource
    private ValueOperations<String,Object> valueOperations;

    //设置过期时长
    public final static long DEFAULT_EXPIRE = 60 * 60 * 24;

    //不设置过期时长
    public final static long NOT_EXPIRE = -1;

    private final static Gson gson = new Gson();

    /**
     * Object转JSON字符串
     * @param obj
     * @return
     */
    private String toJson(Object obj){
        if(obj instanceof Integer || obj instanceof Long || obj instanceof Double || obj instanceof Float || obj instanceof  Boolean || obj instanceof  String){
            return String.valueOf(obj);
        }
        return gson.toJson(obj);
    }

    /**
     * JSON转Object
     * @param json
     * @param clazz
     * @return
     * @param <T>
     */
    private <T> T fromJSON(String json,Class<T> clazz){
        return gson.fromJson(json,clazz);
    }

    /**
     * 设置缓存基本对象、Integer、String、实体类
     * @param key 键
     * @param value 值
     * @param expire 过期时间
     */
    public void set(String key,Object value,long expire){
        valueOperations.set(key,toJson(value));
        if(expire!=NOT_EXPIRE){
            redisTemplate.expire(key,expire, TimeUnit.SECONDS);
        }
    }

    /**
     * 设置缓存基本对象、Integer、String、实体类、默认24小时
     * @param key 键
     * @param value 值
     */
    public void set(String key,Object value){
        set(key,value,DEFAULT_EXPIRE);
    }

    /**
     * 获取基本对象，Integer,String,实体类等
     * @param key 键
     * @param clazz 实体对象
     * @param expire 获取缓存时间
     * @return T
     * @param <T> 泛型
     */
    public <T> T get(String key ,Class<T> clazz,long expire){
        String value = (String) valueOperations.get(key);
        if(expire != NOT_EXPIRE){
            redisTemplate.expire(key,expire,TimeUnit.SECONDS);
        }
        return null == value ? null : fromJSON(value,clazz);
    }

    /**
     * 获取缓存信息、默认没有缓存时间
     * @param key key
     * @param clazz 实体对象
     * @return T
     * @param <T> 泛型
     */
    public <T> T get(String key,Class<T> clazz){
        return get(key,clazz,NOT_EXPIRE);
    }

    /**
     * 删除指定key数据
     * @param key
     */
    public void delete(String key){
        redisTemplate.delete(key);
    }

    /**
     * 判断key是否存在
     * @param key
     * @return
     */
    public boolean haskey(String key){
        return Boolean.TRUE.equals(redisTemplate.hasKey(key));
    }
}
