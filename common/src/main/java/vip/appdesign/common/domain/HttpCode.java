package vip.appdesign.common.domain;

public class HttpCode {

    /**
     * 自定义异常
     */
    public static final int SUN_ERROR = 801;
    /**
     * 成功
     */
    public static final int SUCCESS = 200;
    /**
     * 警告
     */
    public static final int WARN = 201;
    /**
     * 错误
     */
    public static final int ERROR =500;
}
