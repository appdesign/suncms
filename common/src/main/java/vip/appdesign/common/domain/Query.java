package vip.appdesign.common.domain;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.core.metadata.OrderItem;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import vip.appdesign.common.filter.SQLFilter;
import vip.appdesign.common.utils.ConvertUtils;
import vip.appdesign.common.utils.StringUtils;

import java.util.Map;

/**
 * 查询工具类、mybaits-plus封装专用
 * @param <T>
 */
public class Query<T> {
    private int currentPage = 1;
    private int pageSize = 10;
    public IPage getPage(Map<String, Object> params){
        return getPage(params,null,false);
    }
    public IPage<T> getPage(Map<String, Object> params,String defaultOrderField,boolean isAsc) {
        if(params.get(Constant.PAGE) != null){
            currentPage = ConvertUtils.toInt(params.get(Constant.PAGE),currentPage);
        }
        if(params.get(Constant.SIZE) != null){
            pageSize = ConvertUtils.toInt(params.get(Constant.SIZE),pageSize);
        }

        //分页对象
        Page<T> page = new Page<>(currentPage,pageSize);

        //优先判断前端字段是否排序（order_field、order是通过拼接SQL实现排序的，会有SQL注入风险）
        String order_field = SQLFilter.sqlInject(String.valueOf(params.get(Constant.ORDER_FIELD)));
        String order = SQLFilter.sqlInject(String.valueOf(params.get(Constant.ORDER)));
        if(!StringUtils.isEmpty(order_field) && !StringUtils.isEmpty(order)){
            if(order.equals("asc")){
                return page.addOrder(OrderItem.asc(order_field));
            }else {
                return page.addOrder(OrderItem.desc(order_field));
            }
        }
        //判断是否默认排序
        if(StringUtils.isEmpty(defaultOrderField)){
            return page;
        }
        if(isAsc){
            return page.addOrder(OrderItem.asc(defaultOrderField));
        }else {
            return page.addOrder(OrderItem.desc(defaultOrderField));
        }
    }
}
