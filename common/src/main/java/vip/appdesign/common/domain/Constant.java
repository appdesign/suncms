package vip.appdesign.common.domain;

/**
 * 常量
 */
public class Constant {
    //当前页码
    public static final String PAGE = "page";
    //每页大小
    public static final String SIZE = "size";
    //排序字段
    public static final String ORDER_FIELD = "order_field";
    //排序的方法(asc,desc)
    public static final String ORDER = "order";

    //Redis常量
    public static final String REDIS_SYS_CONFIG_KEY = "sys_config:";

    /**
     * 云存储配置KEY
     */
    public static  final String CLOUD_STORAGE_CONFIG_KEY = "CLOUD_STORAGE_CONFIG_KEY";
    public enum CloudService{
        /**
         * 本地
         */
        LOCAL(0),
        /**
         * 七牛云
         */
        QINIU(1),
        /**
         * 阿里云
         */
        ALIYUN(2),
        /**
         * 腾讯云
         */
        TENCENT(3);
        private int value;
        CloudService(int value) {
            this.value =value;
        }
        public int getValue() {
            return value;
        }
    }

    /**
     * 登录配置
     */
    public static  final String LOGIN_LOGIN_CONFIG_KEY = "LOGIN_LOGIN_CONFIG_KEY";
}
