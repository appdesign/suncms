package vip.appdesign.common.domain;

import vip.appdesign.common.utils.StringUtils;

import java.util.HashMap;

/**
 * 公共返回响应类
 * @author sun
 */
public class R extends HashMap<String, Object> {
    private static final long serialVersionUID = 1L;

    /**
     * 状态码
     */
    public static final String CODE_TAG = "code";
    /**
     * 消息体
     */
    public static final String MSG_TAG = "msg";
    /**
     * 对象
     */
    public static final String DATA_TAG = "data";



    /**初始化一个R空对象*/
    public R() {}

    /**
     * 初始化一个R对象
     * @param code
     * @param msg
     */
    public R(int code, String msg) {
        super.put(CODE_TAG, code);
        super.put(MSG_TAG, msg);
    }

    /**
     * 初始化一个R对象
     * @param code
     * @param msg
     * @param data
     */
    public R(int code, String msg, Object data) {
        super.put(CODE_TAG, code);
        super.put(MSG_TAG, msg);
        if(StringUtils.isNotNull(data)){
            super.put(DATA_TAG, data);
        }
    }
    public static R success() {
        return R.success("操作成功");
    }
    /**
     * 返回成功（msg）
     * @param msg
     * @return
     */
    public static R success(String msg) {
        return R.success(msg, null);
    }
    /**
     * 返回成功（data）
     * @param data
     * @return
     */
    public static R success(Object data) {
        return R.success("操作成功",data);
    }
    /**
     * 返回成功（msg+data）
     * @param msg
     * @param data
     * @return
     */
    public static R success(String msg, Object data){
        return new R(HttpCode.SUCCESS, msg, data);
    }

    /**
     * 返回警告(msg)
     * @param msg
     * @return
     */
    public static R warn(String msg) {
        return R.warn(msg, null);
    }

    /**
     * 返回警告（msg+data）
     * @param msg
     * @param data
     * @return
     */
    public static R warn(String msg,Object data) {
        return new R(HttpCode.WARN, msg, data);
    }

    /**
     * 返回错误（msg）
     * @param msg
     * @return
     */
    public static R error(String msg) {
        return R.error(msg, null);
    }

    /**
     * 返回错误（msg+data）
     * @param msg
     * @param data
     * @return
     */
    public static R error(String msg,Object data) {
        return new R(HttpCode.ERROR, msg, data);
    }

    /**
     * 链式调用
     * @param key 键
     * @param value  值
     * @return 对象
     */
    @Override
    public R put(String key, Object value) {
         super.put(key, value);
         return this;
    }
}
