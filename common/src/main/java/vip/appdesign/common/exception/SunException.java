package vip.appdesign.common.exception;

import vip.appdesign.common.domain.HttpCode;

/**
 * 自定义异常
 * @author sun
 */
public class SunException extends RuntimeException{
    private static final long serialVersionUID = 1L;
    private int code = HttpCode.SUN_ERROR;
    private String msg;
    public SunException(String msg) {
        super(msg);
        this.msg = msg;
    }
    public SunException(String msg,Throwable e) {
        super(msg,e);
        this.msg = msg;
    }
    public SunException(int code,String msg ){
        super(msg);
        this.code = code;
        this.msg = msg;
    }
    public  SunException(int code,String msg,Throwable e){
        super(msg,e);
        this.code = code;
        this.msg = msg;
    }

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }
}
