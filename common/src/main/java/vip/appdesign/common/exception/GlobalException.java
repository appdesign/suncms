package vip.appdesign.common.exception;

import jakarta.servlet.http.HttpServletRequest;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.HttpRequestMethodNotSupportedException;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;
import org.springframework.web.method.annotation.MethodArgumentTypeMismatchException;
import org.springframework.web.servlet.NoHandlerFoundException;
import org.springframework.web.servlet.resource.NoResourceFoundException;
import vip.appdesign.common.domain.R;

/**
 * 全局异常处理
 * @author sunpc
 */
@RestControllerAdvice
public class GlobalException {
    private static final Logger log = LoggerFactory.getLogger(GlobalException.class);

    /**
     * 自定义的异常拦截
     * @param e
     * @return
     */
    @ExceptionHandler(SunException.class)
    public R handleBindException(SunException e) {
        log.error(e.getMessage(),e);
        String msg = e.getMessage();
        int code = e.getCode();
        return new R(code,msg);
    }
    /**
     * 访问路径异常拦截
     */
    @ExceptionHandler(NoResourceFoundException.class)
    public R handleNoResourceFoundException(NoResourceFoundException e, HttpServletRequest request) {
        String requestURI = request.getRequestURI();
        log.error("请求路径不存在{},请检查",requestURI,e);
        return R.error(String.format("请求路径[%s]不存在、请检查！",requestURI));
    }
    /**
     * 请求方式不支持拦截
     */
    @ExceptionHandler(HttpRequestMethodNotSupportedException.class)
    public R handleHttpRequestMethodNotSupportedException(HttpRequestMethodNotSupportedException e, HttpServletRequest request) {
        String requestURI = request.getRequestURI();
        log.error("请求地址'{}',不支持'{}'请求", requestURI, e.getMethod());
        return R.error(String.format("请求地址[%s],不支持[%s]请求", requestURI, e.getMethod()));
    }
    /**
     * 参数类型异常拦截
     */
    @ExceptionHandler(MethodArgumentTypeMismatchException.class)
    public R handleMethodArgumentTypeMismatchException(MethodArgumentTypeMismatchException e, HttpServletRequest request) {
        String requestURI = request.getRequestURI();
        log.error("请求参数不匹配'{}',请检查",requestURI,e);
        return R.error(String.format("请求参数类型不匹配参数[%s]要求类型为'%s',但输入的是'%s'",e.getName(),e.getRequiredType().getName(),e.getValue()));
    }
    /**
     * 404异常
     */
    @ExceptionHandler(NoHandlerFoundException.class)
    public R handleNoHandlerFoundException(NoHandlerFoundException e, HttpServletRequest request) {
        String requestURI = request.getRequestURI();
        log.error("请求地址'{}',发生系统异常、无该资源", requestURI, e);
        return R.error(String.format("请求地址[%],发生系统异常404、无该资源", requestURI));
    }
    /**
     * 系统异常
     */
    @ExceptionHandler(Exception.class)
    public R handleException(Exception e, HttpServletRequest request) {
        String requestURI = request.getRequestURI();
        log.error("请求地址'{}',发生系统异常.", requestURI, e);
        return R.error("服务器开小差了",e.getMessage());
    }
}
