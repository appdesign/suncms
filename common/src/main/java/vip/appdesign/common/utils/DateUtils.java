package vip.appdesign.common.utils;

import org.joda.time.DateTime;

import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * 时间工具类
 * @author sun
 */
public class DateUtils {
    public static String YYYY = "yyyy";
    public static String YYYY_MM = "yyyy-MM";
    public static String YYYY_MM_DD = "yyyy-MM-dd";
    public static String YYYY_MM_DD_HH_MM_SS = "yyyy-MM-dd HH:mm:ss";
    public static String YYYYMMDDHHMMSSSSS = "yyyyMMddHHmmssSSS";

    /**
     * 获取当前日期
     * @return new Date()
     */
    public static Date getNowDate(){
        return new Date();
    }
    /**
     * 获取当前日期、格式yyyy-MM-dd
     * @return String
     */
    public static String getDate(){
        return new SimpleDateFormat(YYYY_MM_DD).format(new Date());
    }
    /**
     * 获取当前时间、格式yyyy-MM-dd HH:mm:ss
     * @return String
     */
    public static String getTime(){
        return new SimpleDateFormat(YYYY_MM_DD_HH_MM_SS).format(new Date());
    }
    /**
     * 获取当前时间字符串、格式yyyyMMddHHmmssSSS
     * @return String
     */
    public static String getDateSerial(){
        return new SimpleDateFormat(YYYYMMDDHHMMSSSSS).format(new Date());
    }
    /**
     * 日期格式化
     * @param pattern  格式，如：yyyy-MM-dd
     * @return  返回yyyy-MM-dd格式日期
     */
    public static String format(String pattern) {
        SimpleDateFormat df = new SimpleDateFormat(pattern);
        return df.format(new Date());
    }

    /**
     * 对日期进行加减（分钟数）
     * @param date
     * @param minute
     * @return
     */
    public static Date addDateMinute(Date date, int minute) {
        DateTime dateTime = new DateTime(date);
        return dateTime.plusMinutes(minute).toDate();
    }
}
