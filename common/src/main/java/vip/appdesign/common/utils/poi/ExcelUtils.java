package vip.appdesign.common.utils.poi;

import com.alibaba.excel.EasyExcel;
import com.alibaba.excel.ExcelReader;
import com.alibaba.excel.ExcelWriter;
import com.alibaba.excel.read.metadata.ReadSheet;
import com.alibaba.excel.write.metadata.WriteSheet;
import jakarta.servlet.http.HttpServletResponse;
import org.apache.poi.ss.formula.functions.T;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.DataFormatter;
import org.springframework.web.bind.annotation.RequestBody;
import vip.appdesign.common.exception.SunException;

import java.io.InputStream;
import java.net.URLEncoder;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * Excel工具类
 */
public class ExcelUtils {
    public static void dynamicExport(HttpServletResponse response, LinkedHashMap<String,DynamicExcelData> fieldMap, List<Map<String,Object>> list,String sheetName){
        if(list.isEmpty()){
            throw new SunException("没有表单数据");
        }
        if(fieldMap.isEmpty()){
            throw new SunException("表单字段为空、请指定");
        }
        int size = list.size();
        List<List<String>> dataList = new ArrayList<>();
        for(int i=0;i<size;i++){
            dataList.add(new ArrayList<>());
        }
    }

    /**
     * 导出表格
     * @param excelModel
     * @param response
     */
    public static void dataWrite(@RequestBody ExcelModel excelModel,HttpServletResponse response){
        response.setContentType("application/vnd.openxmlformats-officedocument.spreadsheetml.sheet");
        response.setCharacterEncoding("utf-8");
        try {
            String fileName = URLEncoder.encode(excelModel.getFileName(),"UTF-8");
            response.setHeader("Content-disposition", "attachment;filename=" + fileName + ".xlsx");
            EasyExcel.write(response.getOutputStream()).head(setHead(excelModel.getHeadMap())).sheet(excelModel.getFileName()).doWrite(setData(excelModel.getDatalist(),excelModel.getFieldMap()));
            // 这里需要设置不关闭流
        } catch (Exception e) {
            response.reset();
            response.setContentType("application/json");
            response.setCharacterEncoding("utf-8");
            throw new SunException("下载失败",e);
        }
    }

    /**
     * 设置表格表头
     * @param headMap Stirng[] 指定需要导出的表头
     * @return
     */
    public static List<List<String>> setHead(String[] headMap){
        List<List<String>> list = new ArrayList<List<String>>();
        for (String head : headMap ){
            List<String> h = new ArrayList<String>();
            h.add(head);
            list.add(h);
        }
        return list;
    }

    /**
     * 设置表单数据
     * @param datalist List<Map<String,Object>>格式、方便使用Map集合的数据处理
     * @param fieldMap String[] 指定需要的导出字段的数组
     * @return
     */
    public static List<List<Object>> setData(List<Map<String,Object>> datalist,String[] fieldMap){
        List<List<Object>> lists = new ArrayList<List<Object>>();
        for(Map<String,Object> map : datalist){
            List<Object> list = new ArrayList<Object>();
            for (int i=0;i<fieldMap.length;i++){
                list.add(map.get(fieldMap[i]));
            }
            lists.add(list);
        }
        return lists;
    }

    /**
     * EasyExcel流的方式进行写入
     * @param datalist
     * @param filePath
     * @param sheetName
     */
    public static void writeExcel(List<List<Object>> datalist,String filePath,String sheetName){
        ExcelWriter easyWriter = EasyExcel.write(filePath).build();
        WriteSheet sheet = EasyExcel.writerSheet(sheetName).build();
        for (List<Object> data : datalist){
            List<String> row = data.stream().map(Object::toString).collect(Collectors.toList());
            easyWriter.write(row,sheet);
         }
        easyWriter.finish();
    }

    /**
     * 读取Excel转为list
     * @param inputStream
     * @param head
     * @return
     */
    public static <T> List<T> readExcel(InputStream inputStream,Class<T> head) {
    //  ExcelListener listener = new ExcelListener();
        return EasyExcel.read(inputStream,head,null).autoCloseStream(false).doReadAllSync();
    }

    /**
     * 获取cell的值
     * @param cell
     * @return
     */
    public static String getCellValue(Cell cell) {
        String cellValue = "";
        // 以下是判断数据的类型
        switch (cell.getCellTypeEnum()) {
            case NUMERIC: // 数字
                if (org.apache.poi.ss.usermodel.DateUtil.isCellDateFormatted(cell)) {
                    SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
                    cellValue = sdf.format(org.apache.poi.ss.usermodel.DateUtil.getJavaDate(cell.getNumericCellValue())).toString();
                } else {
                    DataFormatter dataFormatter = new DataFormatter();
                    cellValue = dataFormatter.formatCellValue(cell);
                }
                break;
            case STRING: // 字符串
                cellValue = cell.getStringCellValue();
                break;
            case BOOLEAN: // Boolean
                cellValue = cell.getBooleanCellValue() + "";
                break;
            case FORMULA: // 公式
                cellValue = cell.getCellFormula() + "";
                break;
            case BLANK: // 空值
                cellValue = "";
                break;
            case ERROR: // 故障
                cellValue = "非法字符";
                break;
            default:
                cellValue = "未知类型";
                break;
        }
        return cellValue;
    }

    /**
     * 获取Excel转换为List<T>
     * @param inputStream
     * @param head
     * @return
     * @param <T>
     */
    public static <T> List<T> readExcelToList(InputStream inputStream,Class<T> head){
        //1.创建监听
        ExcelListener<T> excelListener = new ExcelListener<T>();
        //2.获取工作对象输入流
        ExcelReader excelReader = EasyExcel.read(inputStream,head,excelListener).autoCloseStream(false).build();
        //3.获取默认第一张表的输入流
        ReadSheet readSheet = EasyExcel.readSheet(0).build();
        //4.读取信息，每一行都会监听excelLinstener的invoke方法
        excelReader.read(readSheet);
        //5.关闭流，如果不关闭会创建临时文件会导致磁盘占用过大
        excelReader.finish();
        return excelListener.getList();
    }

}
