package vip.appdesign.common.utils.poi;

import com.alibaba.excel.context.AnalysisContext;
import com.alibaba.excel.event.AnalysisEventListener;
import lombok.Getter;
import lombok.extern.slf4j.Slf4j;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * Excel通用读取监听类
 */
@Slf4j
@Getter
public class ExcelListener<T> extends AnalysisEventListener<T> {
    /**
     * 自定义暂时存储data，可以通过实例获取该值
     */
    private final List<T> list = new ArrayList<>();

    /**
     *读取的每一行数据
     */
    @Override
    public void invoke(T data, AnalysisContext analysisContext) {
//        log.info("读取的的数据{}",data);
        list.add(data);
    }

    @Override
    public void invokeHeadMap(Map<Integer, String> headMap, AnalysisContext context) {
        System.out.println("表头信息"+headMap);
    }

    @Override
    public void doAfterAllAnalysed(AnalysisContext analysisContext) {
        log.info("{}条数据，解析完成", list.size());
    }
}
