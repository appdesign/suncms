package vip.appdesign.common.utils;

import java.util.ArrayList;
import java.util.List;

/**
 * 字符串工具类
 * @author sun
 */
public class StringUtils {
    /**
     * 判断对象是否为null
     * @param object
     * @return
     */
    public static boolean isNotNull(Object object) {
        if (object == null) {
            return false;
        }
        return true;
    }

    /**
     * 判断字符串是否为空
     * @param value
     * @return
     */
    public static boolean isEmpty(CharSequence value) {
        return value == null || value.length() == 0;
    }

    /**
     * 数组分割
     */
    public static <T> List<List<T>> partition(List<T> list, int size) {
        List<List<T>> result = new ArrayList<List<T>>();
        for (int i = 0; i < list.size(); i+=size) {
            result.add(new ArrayList<>(list.subList(i, Math.min(i+size, list.size()))));
        }
        return result;
    }

}
