package vip.appdesign.common.utils;

import com.baomidou.mybatisplus.core.metadata.IPage;

import java.io.Serializable;
import java.util.List;

/**
 * 分页工具类
 */
public class PageUtils implements Serializable {
    private static final long serialVersionUID = 1L;
    private int pageSize;
    private int currentPage;
    private int totalPage;
    private int totalCount;
    private List<?> list;

    public int getPageSize() {
        return pageSize;
    }

    public void setPageSize(int pageSize) {
        this.pageSize = pageSize;
    }

    public int getCurrentPage() {
        return currentPage;
    }

    public void setCurrentPage(int currentPage) {
        this.currentPage = currentPage;
    }

    public int getTotalPage() {
        return totalPage;
    }

    public void setTotalPage(int totalPage) {
        this.totalPage = totalPage;
    }

    public int getTotalCount() {
        return totalCount;
    }

    public void setTotalCount(int totalCount) {
        this.totalCount = totalCount;
    }

    public List<?> getList() {
        return list;
    }

    public void setList(List<?> list) {
        this.list = list;
    }

    /**
     * 分页数据初始化
     * @param list
     * @param pageSize
     * @param currentPage
     * @param totalCount
     */
    public PageUtils(List<?> list, int pageSize, int currentPage, int totalCount) {
        this.list = list;
        this.pageSize = pageSize;
        this.currentPage = currentPage;
        this.totalCount = totalCount;
        this.totalPage = (int)Math.ceil((double)totalCount/ pageSize);
    }

    /**
     * 分页数据提取
     * @param page IPage<?>
     */
    public PageUtils(IPage<?> page) {
        this.list = page.getRecords();
        this.pageSize = (int) page.getSize();
        this.currentPage = (int) page.getCurrent();
        this.totalCount = (int) page.getTotal();
        this.totalPage = (int)page.getPages();
    }
}
