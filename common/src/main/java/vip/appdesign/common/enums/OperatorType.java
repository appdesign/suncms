package vip.appdesign.common.enums;

public enum OperatorType {
    /**
     * PC 前台
     */
    PC,
    /**
     * APP app
     */
    APP,
    /**
     * ADMIN 后台
     */
    ADMIN,
    /**
     * 未知
     */
    OTHER
}
