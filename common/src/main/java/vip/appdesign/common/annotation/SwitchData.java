package vip.appdesign.common.annotation;

import vip.appdesign.common.enums.DataSourceType;

import java.lang.annotation.*;

/**
 * 切换数据源
 */
@Target({ElementType.METHOD,ElementType.TYPE})
@Retention(RetentionPolicy.RUNTIME)
@Documented
@Inherited
public @interface SwitchData {
    public DataSourceType value() default DataSourceType.MASTER;
}
