package vip.appdesign.common.annotation;

import vip.appdesign.common.enums.OperatorType;

import java.lang.annotation.*;

/**
 * 系统日志注解
 * @author sun
 */
@Target(ElementType.METHOD)
@Retention(RetentionPolicy.RUNTIME)
@Documented
public @interface Log {
    /**
     * 操作描述
     * @return
     */
    String value() default "";

    public OperatorType operatorType() default OperatorType.OTHER;

}
