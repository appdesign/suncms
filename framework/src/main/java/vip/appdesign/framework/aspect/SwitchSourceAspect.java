package vip.appdesign.framework.aspect;

import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Pointcut;
import org.aspectj.lang.reflect.MethodSignature;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.core.annotation.AnnotationUtils;
import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Component;
import vip.appdesign.common.annotation.SwitchData;
import vip.appdesign.common.enums.DataSourceType;
import vip.appdesign.framework.config.druid.DynamicDataSource;
import java.lang.reflect.Method;

@Aspect
@Order(1)
@Component
public class SwitchSourceAspect {
    private Logger logger = LoggerFactory.getLogger(SwitchSourceAspect.class);

    @Pointcut("@annotation(vip.appdesign.common.annotation.SwitchData) || @within(vip.appdesign.common.annotation.SwitchData) ")
    public void pointCut(){

    }
    @Around("pointCut()")
    public Object around(ProceedingJoinPoint joinPoint) throws Throwable {
        MethodSignature signature = (MethodSignature) joinPoint.getSignature();
        Method method = signature.getMethod();
        SwitchData switchData = method.getAnnotation(SwitchData.class);
        if(switchData == null){
            DynamicDataSource.setDataSource(DataSourceType.MASTER);
        }else {
            DynamicDataSource.setDataSource(switchData.value());
        }
        try {
            return joinPoint.proceed();
        }finally {
            DynamicDataSource.clearDataSource();
        }
    }

}
