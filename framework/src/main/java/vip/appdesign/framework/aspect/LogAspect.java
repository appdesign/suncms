package vip.appdesign.framework.aspect;

import com.google.gson.Gson;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Pointcut;
import org.aspectj.lang.reflect.MethodSignature;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import vip.appdesign.common.annotation.Log;
import vip.appdesign.common.exception.SunException;
import vip.appdesign.common.utils.http.HttpContextUtils;
import vip.appdesign.framework.system.domain.SysLog;
import vip.appdesign.framework.system.service.SysLogService;
import java.lang.reflect.Method;
import java.util.Date;

/**
 * 操作日志、切面处理
 */
@Aspect
@Component
public class LogAspect {
    @Autowired
    private SysLogService sysLogService;
    @Pointcut("@annotation(vip.appdesign.common.annotation.Log)")
    public void logPointcut(){}
    @Around("logPointcut()")
    public Object logAround(ProceedingJoinPoint joinPoint) throws Throwable {
        long startTime = System.currentTimeMillis();
        Object result = joinPoint.proceed();
        long endTime = System.currentTimeMillis();

        //执行时长
        long time = endTime - startTime;
        saveSysLog(joinPoint,time);
        return  result;
    }

    private void saveSysLog(ProceedingJoinPoint joinPoint,long time){
        MethodSignature methodSignature = (MethodSignature) joinPoint.getSignature();
        Method method = methodSignature.getMethod();
        SysLog sysLog = new SysLog();
        Log log = method.getAnnotation(Log.class);
        //获取注解上的描述
        if(log!=null){
            sysLog.setOperation(log.value());
        }
        //获取方法名称
        String className = joinPoint.getTarget().getClass().getName();
        String methodName = method.getName();
        sysLog.setMethod(className+"."+methodName+"()");

        //请求参数
        Object[] args = joinPoint.getArgs();
        try {
            String params = new Gson().toJson(args);
            sysLog.setParams(params);
        }catch (Exception e){
            throw new SunException("Log请求参数转换JSON格式错误",e);
        }
        //获取IP地址
        sysLog.setIp(HttpContextUtils.getIpAddr());
        //获取用户名
        sysLog.setUsername("sys");
        sysLog.setTime(time);
        sysLog.setCreateTime(new Date());

        //保存系统日志
        sysLogService.save(sysLog);
    }

}
