package vip.appdesign.framework.file;

/**
 * 媒体类型
 */
public class MIMEType {
    public static final String APPLICATION_XML = "application/xml";
    public static final String APPLICATION_JSON = "application/json";
    public static final String IMAGE_PNG = "image/png";
    public static final String IMAGE_JPG = "image/jpg";
    public static final String IMAGE_JPEG = "image/jpeg";
    public static final String IMAGE_GIF = "image/gif";
    public static final String IMAGE_BMP = "image/bmp";

    public static final String[] IMAGE_EXTENSION = {"jpg", "jpeg", "gif", "bmp", "png"};
    public static final String[] FLASH_EXTENSION = {"flash", "fl", "flv","swf"};
    public static final String[] MEDIA_EXTENSION = { "swf", "flv", "mp3", "wav", "wma", "wmv", "mid", "avi", "mpg","asf", "rm", "rmvb" };
    public static final String[] VIDEO_EXTENSION =  { "mp4", "avi", "rmvb" };
    public static final String[] DEFAULT_ALLOWED_EXTENSION = {
            // 图片
            "bmp", "gif", "jpg", "jpeg", "png",
            // word excel powerpoint
            "doc", "docx", "xls", "xlsx", "ppt", "pptx", "html", "htm", "txt",
            // 压缩文件
            "rar", "zip", "gz", "bz2",
            // 视频格式
            "mp4", "avi", "rmvb",
            // pdf
            "pdf"
    };

    /**
     * 获取媒体类型
     * @param mimeType
     * @return
     */
    public static String getExtension(String mimeType) {
        switch (mimeType){
            case APPLICATION_XML:
                return "xml";
            case APPLICATION_JSON:
                return "json";
            case IMAGE_PNG:
                return "png";
            case IMAGE_JPG:
                return "jpg";
            case IMAGE_JPEG:
                return "jpeg";
            case IMAGE_BMP:
                return "bmp";
            case IMAGE_GIF:
                return "gif";
            default:
                return "";
        }
    }
}
