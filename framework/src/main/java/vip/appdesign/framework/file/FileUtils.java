package vip.appdesign.framework.file;

import org.apache.commons.io.FilenameUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.multipart.MultipartFile;
import vip.appdesign.common.domain.Constant;
import vip.appdesign.common.exception.SunException;
import vip.appdesign.framework.config.SunCmsConfig;
import vip.appdesign.framework.oss.cloud.CloudStorageConfig;
import vip.appdesign.framework.oss.cloud.OSSFactory;

import java.io.File;
import java.io.IOException;
import java.util.Calendar;
import java.util.Objects;
import java.util.UUID;

/**
 * 文件工具类
 */
public class FileUtils {


    /**
     * 默认10M限制
     */
    public static final long DEFAULT_MAX_SIZE = 10 * 1024 * 1024;
    /**
     * 文件名长度限制、默认100个字符
     */
    public static final long DEFAULT_FILENAME_LIMIT = 100;

    public static String upload(int type,String module, MultipartFile file,String[] allowedExtension) {
        //获取文件大小
        long fileSize = file.getSize();
        long fileNameLength = file.getOriginalFilename().length();

        if(fileSize>DEFAULT_MAX_SIZE){
            throw new SunException("超出文件上传过大限制："+DEFAULT_FILENAME_LIMIT/1024/1024+"MB");
        }
        if(fileNameLength > DEFAULT_FILENAME_LIMIT) {
            throw new SunException("文件名称过长、限制："+DEFAULT_FILENAME_LIMIT+"个字符");
        }

        //获取文件后缀
         String extension = FilenameUtils.getExtension(file.getOriginalFilename());
        if(StringUtils.isEmpty(extension)){
            extension = MIMEType.getExtension(Objects.requireNonNull(file.getContentType()));
        }
        if(!isAllowedExtension(extension,allowedExtension)){
            throw new SunException("只支持上传"+allowedExtension.toString()+"的类型、当前为："+extension+"、请检查！");
        }
        String newFileName = UUID.randomUUID().toString().replace("-", "")+"."+extension;

        Calendar calendar = Calendar.getInstance();
        String year = String.valueOf(calendar.get(Calendar.YEAR));
        String month = String.valueOf(calendar.get(Calendar.MONTH)+1);
        String day = String.valueOf(calendar.get(Calendar.DAY_OF_MONTH));
        String relativePath = module+"/"+year+"/"+month+"/"+day+"/"+newFileName;
        String absolutePath = SunCmsConfig.getUpload()+"/"+relativePath;
        if(type == 0){
            try {
                file.transferTo(getAbsoluteFile(absolutePath));
                return relativePath;
            } catch (IOException e) {
                throw new SunException("本地上传异常",e);
            }
        }else {
            try {
               return OSSFactory.build().upload(file.getBytes(),relativePath);
            } catch (IOException e) {
                throw new SunException("云存储上传异常",e);
            }
        }
    }


    /**
     * 判断MIME是否允许的MIME类型
     * @param extension
     * @param allowedExtension
     * @return
     */
    public static final boolean isAllowedExtension(String extension,String[] allowedExtension){
        for(String str : allowedExtension){
            if(str.equalsIgnoreCase(extension)){
                return true;
            }
        }
        return  false;
    }


    /**
     * 获取绝对路径的文件本地存储
     * @param absolutePath 绝对路径
     * @return
     */
    public static final File getAbsoluteFile(String absolutePath){

        File file = new File(absolutePath);
        if(!file.exists()){
            if(!file.getParentFile().exists()){
                file.getParentFile().mkdirs();
            }
        }
        return file;
    }
}
