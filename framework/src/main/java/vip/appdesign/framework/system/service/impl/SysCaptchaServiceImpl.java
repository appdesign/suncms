package vip.appdesign.framework.system.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.google.code.kaptcha.Producer;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import vip.appdesign.common.exception.SunException;
import vip.appdesign.common.utils.DateUtils;
import vip.appdesign.framework.system.domain.SysCaptcha;
import vip.appdesign.framework.system.service.SysCaptchaService;
import vip.appdesign.framework.system.mapper.SysCaptchaMapper;
import org.springframework.stereotype.Service;

import java.awt.image.BufferedImage;
import java.util.Date;

/**
* @author sunpc
* @description 针对表【sys_captcha(系统验证码)】的数据库操作Service实现
* @createDate 2024-07-17 14:49:46
*/
@Service
public class SysCaptchaServiceImpl extends ServiceImpl<SysCaptchaMapper, SysCaptcha>
    implements SysCaptchaService{
    @Autowired
    private Producer producer;
    @Override
    public BufferedImage getCaptcha(String uuid) {
        if(StringUtils.isBlank(uuid)){
            throw new SunException("uuid不能为空");
        }
        //生成验证码文字
        String code = producer.createText();
        SysCaptcha sysCaptcha = new SysCaptcha();
        sysCaptcha.setUuid(uuid);
        sysCaptcha.setCode(code);
        sysCaptcha.setExpireTime(DateUtils.addDateMinute(new Date(),2));
        this.save(sysCaptcha);
        return producer.createImage(code);
    }

    @Override
    public boolean checkCaptcha(String uuid, String code) {
        SysCaptcha sysCaptcha = this.getOne(new QueryWrapper<SysCaptcha>().eq("uuid", uuid).eq("code", code));
        if(sysCaptcha == null){
            return false;
        }
        //删除验证码
        this.removeById(uuid);
        if(sysCaptcha.getCode().equalsIgnoreCase(code) && sysCaptcha.getExpireTime().getTime() >= System.currentTimeMillis()){
            return true;
        }
        return false;
    }
}




