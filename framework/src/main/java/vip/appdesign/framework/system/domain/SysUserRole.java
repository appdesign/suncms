package vip.appdesign.framework.system.domain;

import java.io.Serializable;

import com.baomidou.mybatisplus.annotation.TableId;
import lombok.Data;

/**
 * 用户对应的角色表
 * @TableName sys_user_role
 */
@Data
public class SysUserRole implements Serializable {
    /**
     * 
     */
    @TableId
    private Long id;

    /**
     * 用户ID
     */
    private Long userId;

    /**
     * 角色ID
     */
    private Long roleId;

    private static final long serialVersionUID = 1L;
}