package vip.appdesign.framework.system.service;

import vip.appdesign.framework.system.domain.TzUser;
import com.baomidou.mybatisplus.extension.service.IService;

/**
* @author sunpc
* @description 针对表【tz_user(台账用户表)】的数据库操作Service
* @createDate 2024-07-17 14:49:46
*/
public interface TzUserService extends IService<TzUser> {

}
