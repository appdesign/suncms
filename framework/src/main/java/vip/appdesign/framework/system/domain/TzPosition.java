package vip.appdesign.framework.system.domain;

import java.io.Serializable;

import com.baomidou.mybatisplus.annotation.TableId;
import lombok.Data;

/**
 * 台账设备坐标表
 * @TableName tz_position
 */
@Data
public class TzPosition implements Serializable {
    /**
     * 
     */
    @TableId
    private Long id;

    /**
     * 用户ID
     */
    private Long userId;

    /**
     * 设备ID
     */
    private Long deviceId;

    /**
     * 所属区域
     */
    private String areaName;

    /**
     * 坐标位置
     */
    private String areaPosition;

    private static final long serialVersionUID = 1L;
}