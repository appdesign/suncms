package vip.appdesign.framework.system.mapper;

import vip.appdesign.framework.system.domain.SysConfig;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
* @author sunpc
* @description 针对表【sys_config(系统配置表)】的数据库操作Mapper
* @createDate 2024-07-17 14:49:46
* @Entity vip.appdesign.framework.system.domain.SysConfig
*/
public interface SysConfigMapper extends BaseMapper<SysConfig> {
    SysConfig queryByKey(String key);

    void updateValueByKey(String key, String value);
}




