package vip.appdesign.framework.system.domain;

import java.io.Serializable;

import com.baomidou.mybatisplus.annotation.TableId;
import lombok.Data;

/**
 * 系统配置表
 * @TableName sys_config
 */
@Data
public class SysConfig implements Serializable {
    /**
     * 
     */
    @TableId
    private Long id;

    /**
     * 配置Key
     */
    private String configKey;

    /**
     * 配置value
     */
    private String configValue;

    /**
     * 状态：0隐藏 1显示
     */
    private Integer status;

    /**
     * 备注
     */
    private String remark;

    private static final long serialVersionUID = 1L;
}