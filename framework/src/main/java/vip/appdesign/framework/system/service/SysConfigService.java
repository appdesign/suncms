package vip.appdesign.framework.system.service;

import org.springframework.stereotype.Service;
import vip.appdesign.common.utils.PageUtils;
import vip.appdesign.framework.system.domain.SysConfig;
import com.baomidou.mybatisplus.extension.service.IService;

import java.util.Map;

/**
* @author sunpc
* @description 针对表【sys_config(系统配置表)】的数据库操作Service
* @createDate 2024-07-17 14:49:46
*/

public interface SysConfigService extends IService<SysConfig> {
    /**
     * 查询配置参数
      * @param params
     * @return
     */

    PageUtils queryPage(Map<String, Object> params);

    /**
     * 保存配置
     * @param sysConfig
     */
    public void saveSysConfig(SysConfig sysConfig);

    /**
     * 更新配置
     * @param sysConfig
     */
    public void updateSysConfig(SysConfig sysConfig);

    /**
     * 根据KEY更新Value
     * @param key
     * @param value
     */
    public void updateValueByKey(String key, String value);

    /**
     * 批量删除
     * @param ids
     */
    public void deleteBatch(Long[] ids);
    /**
     * 根据Key获取value值
     * @param key
     * @return
     */
    public String getValue(String key);

    /**
     * 根据key获取value对象
     * @param key
     * @param clazz
     * @return
     * @param <T>
     */
    public <T> T getConfigObject(String key,Class<T> clazz);
}
