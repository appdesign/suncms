package vip.appdesign.framework.system.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import vip.appdesign.framework.system.domain.SysRole;
import vip.appdesign.framework.system.service.SysRoleService;
import vip.appdesign.framework.system.mapper.SysRoleMapper;
import org.springframework.stereotype.Service;

/**
* @author sunpc
* @description 针对表【sys_role(角色)】的数据库操作Service实现
* @createDate 2024-07-17 14:49:46
*/
@Service
public class SysRoleServiceImpl extends ServiceImpl<SysRoleMapper, SysRole>
    implements SysRoleService{

}




