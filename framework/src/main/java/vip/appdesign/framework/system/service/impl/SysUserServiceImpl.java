package vip.appdesign.framework.system.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import vip.appdesign.framework.system.domain.SysUser;
import vip.appdesign.framework.system.service.SysUserService;
import vip.appdesign.framework.system.mapper.SysUserMapper;
import org.springframework.stereotype.Service;

/**
* @author sunpc
* @description 针对表【sys_user(系统用户)】的数据库操作Service实现
* @createDate 2024-07-17 14:49:46
*/
@Service
public class SysUserServiceImpl extends ServiceImpl<SysUserMapper, SysUser>
    implements SysUserService{

    @Override
    public SysUser queryByUsername(String username) {
        return baseMapper.queryByUserName(username);
    }
}




