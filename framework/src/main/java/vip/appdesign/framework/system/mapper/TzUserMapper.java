package vip.appdesign.framework.system.mapper;

import vip.appdesign.framework.system.domain.TzUser;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
* @author sunpc
* @description 针对表【tz_user(台账用户表)】的数据库操作Mapper
* @createDate 2024-07-17 14:49:46
* @Entity vip.appdesign.framework.system.domain.TzUser
*/
public interface TzUserMapper extends BaseMapper<TzUser> {

}




