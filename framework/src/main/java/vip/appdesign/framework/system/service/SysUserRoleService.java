package vip.appdesign.framework.system.service;

import vip.appdesign.framework.system.domain.SysUserRole;
import com.baomidou.mybatisplus.extension.service.IService;

/**
* @author sunpc
* @description 针对表【sys_user_role(用户对应的角色表)】的数据库操作Service
* @createDate 2024-07-17 14:49:46
*/
public interface SysUserRoleService extends IService<SysUserRole> {

}
