package vip.appdesign.framework.system.service;

import vip.appdesign.framework.system.domain.SysUser;
import com.baomidou.mybatisplus.extension.service.IService;

/**
* @author sunpc
* @description 针对表【sys_user(系统用户)】的数据库操作Service
* @createDate 2024-07-17 14:49:46
*/
public interface SysUserService extends IService<SysUser> {
    /**
     * 根据用户名查询系统用户
     */
    SysUser queryByUsername(String username);
}
