package vip.appdesign.framework.system.mapper;

import vip.appdesign.framework.system.domain.TzPosition;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
* @author sunpc
* @description 针对表【tz_position(台账设备坐标表)】的数据库操作Mapper
* @createDate 2024-07-17 14:49:46
* @Entity vip.appdesign.framework.system.domain.TzPosition
*/
public interface TzPositionMapper extends BaseMapper<TzPosition> {

}




