package vip.appdesign.framework.system.service;

import vip.appdesign.framework.system.domain.TzDevice;
import com.baomidou.mybatisplus.extension.service.IService;

/**
* @author sunpc
* @description 针对表【tz_device(台账设备坐标表)】的数据库操作Service
* @createDate 2024-07-17 14:49:46
*/
public interface TzDeviceService extends IService<TzDevice> {

}
