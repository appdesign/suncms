package vip.appdesign.framework.system.service;

import vip.appdesign.framework.system.domain.SysMenu;
import com.baomidou.mybatisplus.extension.service.IService;

/**
* @author sunpc
* @description 针对表【sys_menu(系统菜单管理)】的数据库操作Service
* @createDate 2024-07-17 14:49:46
*/
public interface SysMenuService extends IService<SysMenu> {

}
