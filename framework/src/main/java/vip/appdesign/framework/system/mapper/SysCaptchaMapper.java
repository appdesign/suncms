package vip.appdesign.framework.system.mapper;

import vip.appdesign.framework.system.domain.SysCaptcha;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
* @author sunpc
* @description 针对表【sys_captcha(系统验证码)】的数据库操作Mapper
* @createDate 2024-07-17 14:49:46
* @Entity vip.appdesign.framework.system.domain.SysCaptcha
*/
public interface SysCaptchaMapper extends BaseMapper<SysCaptcha> {

}




