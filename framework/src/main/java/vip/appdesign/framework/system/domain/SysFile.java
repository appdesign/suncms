package vip.appdesign.framework.system.domain;

import java.io.Serializable;
import java.util.Date;

import com.baomidou.mybatisplus.annotation.TableId;
import lombok.Data;

/**
 * 文件存储地址表
 * @TableName sys_file
 */
@Data
public class SysFile implements Serializable {
    /**
     * 
     */
    @TableId
    private Long id;

    /**
     * URL地址
     */
    private String url;

    /**
     * 文件类型：0本地,1七牛云,2阿里云,3腾讯云
     */
    private Integer type;

    /**
     * 软删除: 默认0,否则是删除时间
     */
    private Integer isDel;

    /**
     * 创建时间
     */
    private Date createTime;

    private static final long serialVersionUID = 1L;
}