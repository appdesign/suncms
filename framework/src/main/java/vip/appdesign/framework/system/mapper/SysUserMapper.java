package vip.appdesign.framework.system.mapper;

import vip.appdesign.framework.system.domain.SysUser;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
* @author sunpc
* @description 针对表【sys_user(系统用户)】的数据库操作Mapper
* @createDate 2024-07-17 14:49:46
* @Entity vip.appdesign.framework.system.domain.SysUser
*/
public interface SysUserMapper extends BaseMapper<SysUser> {

    SysUser queryByUserName(String username);
}




