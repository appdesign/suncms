package vip.appdesign.framework.system.mapper;

import vip.appdesign.framework.system.domain.SysUserRole;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
* @author sunpc
* @description 针对表【sys_user_role(用户对应的角色表)】的数据库操作Mapper
* @createDate 2024-07-17 14:49:46
* @Entity vip.appdesign.framework.system.domain.SysUserRole
*/
public interface SysUserRoleMapper extends BaseMapper<SysUserRole> {

}




