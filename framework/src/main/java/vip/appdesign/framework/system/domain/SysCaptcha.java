package vip.appdesign.framework.system.domain;

import java.io.Serializable;
import java.util.Date;

import com.baomidou.mybatisplus.annotation.TableId;
import lombok.Data;

/**
 * 系统验证码
 * @TableName sys_captcha
 */
@Data
public class SysCaptcha implements Serializable {
    /**
     * uuid
     */
    @TableId
    private String uuid;

    /**
     * 验证码
     */
    private String code;

    /**
     * 过期时间
     */
    private Date expireTime;

    private static final long serialVersionUID = 1L;
}