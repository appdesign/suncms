package vip.appdesign.framework.system.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.apache.commons.lang3.StringUtils;
import vip.appdesign.common.domain.Query;
import vip.appdesign.common.utils.PageUtils;
import vip.appdesign.framework.system.domain.SysLog;
import vip.appdesign.framework.system.service.SysLogService;
import vip.appdesign.framework.system.mapper.SysLogMapper;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;

/**
* @author sunpc
* @description 针对表【sys_log(系统日志表)】的数据库操作Service实现
* @createDate 2024-07-17 14:49:46
*/
@Service
public class SysLogServiceImpl extends ServiceImpl<SysLogMapper, SysLog>
    implements SysLogService{

    @Override
    public PageUtils queryPage(Map<String, Object> params) {
        String key = (String)params.get("key");
        IPage<SysLog> page = this.page(new Query<SysLog>().getPage(params),new QueryWrapper<SysLog>()
                .like(StringUtils.isNotBlank(key),"username",key)
        );
        return new PageUtils(page);
    }

    @Override
    public List<Map<String,Object>> queryAllLog(Map<String, Object> params) {
        return baseMapper.selectMaps(new QueryWrapper<>());
    }

}




