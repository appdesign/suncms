package vip.appdesign.framework.system.service;

import vip.appdesign.common.utils.PageUtils;
import vip.appdesign.framework.system.domain.SysLog;
import com.baomidou.mybatisplus.extension.service.IService;

import java.util.List;
import java.util.Map;

/**
* @author sunpc
* @description 针对表【sys_log(系统日志表)】的数据库操作Service
* @createDate 2024-07-17 14:49:46
*/
public interface SysLogService extends IService<SysLog> {
    PageUtils queryPage(Map<String, Object> params);
    List<Map<String,Object>> queryAllLog(Map<String, Object> params);
}
