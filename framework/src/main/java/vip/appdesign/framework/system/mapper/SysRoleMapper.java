package vip.appdesign.framework.system.mapper;

import vip.appdesign.framework.system.domain.SysRole;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
* @author sunpc
* @description 针对表【sys_role(角色)】的数据库操作Mapper
* @createDate 2024-07-17 14:49:46
* @Entity vip.appdesign.framework.system.domain.SysRole
*/
public interface SysRoleMapper extends BaseMapper<SysRole> {

}




