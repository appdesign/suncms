package vip.appdesign.framework.system.service;

import vip.appdesign.framework.system.domain.SysCaptcha;
import com.baomidou.mybatisplus.extension.service.IService;

import java.awt.image.BufferedImage;

/**
* @author sunpc
* @description 针对表【sys_captcha(系统验证码)】的数据库操作Service
* @createDate 2024-07-17 14:49:46
*/
public interface SysCaptchaService extends IService<SysCaptcha> {
    /**
     * 获取验证码图片
     * @param uuid
     * @return
     */
    BufferedImage getCaptcha(String uuid);

    /**
     * 验证码校验
     * @param uuid
     * @param code
     * @return
     */
    boolean checkCaptcha(String uuid, String code);
}
