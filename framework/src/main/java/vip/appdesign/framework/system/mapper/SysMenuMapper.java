package vip.appdesign.framework.system.mapper;

import vip.appdesign.framework.system.domain.SysMenu;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
* @author sunpc
* @description 针对表【sys_menu(系统菜单管理)】的数据库操作Mapper
* @createDate 2024-07-17 14:49:46
* @Entity vip.appdesign.framework.system.domain.SysMenu
*/
public interface SysMenuMapper extends BaseMapper<SysMenu> {

}




