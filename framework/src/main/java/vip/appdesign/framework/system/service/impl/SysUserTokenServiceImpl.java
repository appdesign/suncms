package vip.appdesign.framework.system.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import vip.appdesign.framework.system.domain.SysUserToken;
import vip.appdesign.framework.system.service.SysUserTokenService;
import vip.appdesign.framework.system.mapper.SysUserTokenMapper;
import org.springframework.stereotype.Service;

/**
* @author sunpc
* @description 针对表【sys_user_token(Token表)】的数据库操作Service实现
* @createDate 2024-07-17 14:49:46
*/
@Service
public class SysUserTokenServiceImpl extends ServiceImpl<SysUserTokenMapper, SysUserToken>
    implements SysUserTokenService{

}




