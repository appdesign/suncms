package vip.appdesign.framework.system.domain;

import java.io.Serializable;

import com.baomidou.mybatisplus.annotation.TableId;
import lombok.Data;

/**
 * 台账用户表
 * @TableName tz_user
 */
@Data
public class TzUser implements Serializable {
    /**
     * 
     */
    @TableId
    private Long id;

    /**
     * 用户ID
     */
    private Long userId;

    /**
     * 用名称
     */
    private String userName;

    /**
     * 归属部门
     */
    private String deptName;

    /**
     * 状态：0离职,1在职
     */
    private Integer status;

    private static final long serialVersionUID = 1L;
}