package vip.appdesign.framework.system.service;

import vip.appdesign.framework.system.domain.SysRole;
import com.baomidou.mybatisplus.extension.service.IService;

/**
* @author sunpc
* @description 针对表【sys_role(角色)】的数据库操作Service
* @createDate 2024-07-17 14:49:46
*/
public interface SysRoleService extends IService<SysRole> {

}
