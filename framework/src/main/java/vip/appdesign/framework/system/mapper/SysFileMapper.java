package vip.appdesign.framework.system.mapper;

import vip.appdesign.framework.system.domain.SysFile;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
* @author sunpc
* @description 针对表【sys_file(文件存储地址表)】的数据库操作Mapper
* @createDate 2024-07-17 14:49:46
* @Entity vip.appdesign.framework.system.domain.SysFile
*/
public interface SysFileMapper extends BaseMapper<SysFile> {

}




