package vip.appdesign.framework.system.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.google.gson.Gson;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import vip.appdesign.common.domain.Query;
import vip.appdesign.common.exception.SunException;
import vip.appdesign.common.utils.PageUtils;
import vip.appdesign.framework.config.SunCmsConfig;
import vip.appdesign.framework.config.SysConfigRedis;
import vip.appdesign.framework.system.domain.SysConfig;
import vip.appdesign.framework.system.service.SysConfigService;
import vip.appdesign.framework.system.mapper.SysConfigMapper;
import org.springframework.stereotype.Service;

import java.util.Arrays;
import java.util.Map;

/**
* @author sunpc
* @description 针对表【sys_config(系统配置表)】的数据库操作Service实现
* @createDate 2024-07-17 14:49:46
*/
@Service("sysConfigService")
public class SysConfigServiceImpl extends ServiceImpl<SysConfigMapper, SysConfig>
    implements SysConfigService{

    @Autowired
    private SysConfigRedis sysConfigRedis;


    @Override
    public PageUtils queryPage(Map<String, Object> params) {
       String configKey = (String) params.get("configKey");
       IPage<SysConfig> page = this.page(new Query<SysConfig>().getPage(params), new QueryWrapper<SysConfig>().like(StringUtils.isNotBlank(configKey), "config_key", configKey).eq("status",1));
       return new PageUtils(page);

    }

    @Override
    public void saveSysConfig(SysConfig sysConfig) {
        this.save(sysConfig);
        if (SunCmsConfig.getRedisOpen()) {
            sysConfigRedis.saveOrUpdate(sysConfig);
        }

    }

    @Override
    public void updateSysConfig(SysConfig sysConfig) {
        this.updateById(sysConfig);
        if(SunCmsConfig.getRedisOpen()){
            sysConfigRedis.saveOrUpdate(sysConfig);
        }
    }

    @Override
    public void updateValueByKey(String key, String value) {
        baseMapper.updateValueByKey(key,value);
        if(SunCmsConfig.getRedisOpen()) {
            sysConfigRedis.delete(key);
        }
    }

    @Override
    public void deleteBatch(Long[] ids) {
        for (Long id : ids) {
            SysConfig sysConfig = this.getById(id);
            if(SunCmsConfig.getRedisOpen()){
                sysConfigRedis.delete(sysConfig.getConfigKey());
            }
        }
        //批量删除ID
        this.removeBatchByIds(Arrays.asList(ids));
    }

    @Override
    public String getValue(String key) {
        if(SunCmsConfig.getRedisOpen()){
            SysConfig sysConfig = sysConfigRedis.get(key);
            if(sysConfig == null){
                sysConfig = baseMapper.queryByKey(key);
                sysConfigRedis.saveOrUpdate(sysConfig);
            }
            return sysConfig == null ? null : sysConfig.getConfigValue();
        }else {
           SysConfig sysConfig = baseMapper.queryByKey(key);
           return sysConfig.getConfigValue();
        }

    }

    @Override
    public <T> T getConfigObject(String key, Class<T> clazz) {
        String value = getValue(key);
        if(StringUtils.isNotBlank(value)){
            return new Gson().fromJson(value,clazz);
        }
        try {
            return clazz.newInstance();
        }catch (Exception e){
            throw new SunException("获取配置参数key："+key);
        }

    }
}




