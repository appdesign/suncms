package vip.appdesign.framework.system.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import vip.appdesign.framework.system.domain.SysUserRole;
import vip.appdesign.framework.system.service.SysUserRoleService;
import vip.appdesign.framework.system.mapper.SysUserRoleMapper;
import org.springframework.stereotype.Service;

/**
* @author sunpc
* @description 针对表【sys_user_role(用户对应的角色表)】的数据库操作Service实现
* @createDate 2024-07-17 14:49:46
*/
@Service
public class SysUserRoleServiceImpl extends ServiceImpl<SysUserRoleMapper, SysUserRole>
    implements SysUserRoleService{

}




