package vip.appdesign.framework.system.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import vip.appdesign.framework.system.domain.TzUser;
import vip.appdesign.framework.system.service.TzUserService;
import vip.appdesign.framework.system.mapper.TzUserMapper;
import org.springframework.stereotype.Service;

/**
* @author sunpc
* @description 针对表【tz_user(台账用户表)】的数据库操作Service实现
* @createDate 2024-07-17 14:49:46
*/
@Service
public class TzUserServiceImpl extends ServiceImpl<TzUserMapper, TzUser>
    implements TzUserService{

}




