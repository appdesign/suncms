package vip.appdesign.framework.system.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import vip.appdesign.framework.system.domain.SysMenu;
import vip.appdesign.framework.system.service.SysMenuService;
import vip.appdesign.framework.system.mapper.SysMenuMapper;
import org.springframework.stereotype.Service;

/**
* @author sunpc
* @description 针对表【sys_menu(系统菜单管理)】的数据库操作Service实现
* @createDate 2024-07-17 14:49:46
*/
@Service
public class SysMenuServiceImpl extends ServiceImpl<SysMenuMapper, SysMenu>
    implements SysMenuService{

}




