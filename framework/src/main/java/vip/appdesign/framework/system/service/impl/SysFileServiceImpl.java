package vip.appdesign.framework.system.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import vip.appdesign.framework.system.domain.SysFile;
import vip.appdesign.framework.system.service.SysFileService;
import vip.appdesign.framework.system.mapper.SysFileMapper;
import org.springframework.stereotype.Service;

/**
* @author sunpc
* @description 针对表【sys_file(文件存储地址表)】的数据库操作Service实现
* @createDate 2024-07-17 14:49:46
*/
@Service
public class SysFileServiceImpl extends ServiceImpl<SysFileMapper, SysFile>
    implements SysFileService{

}




