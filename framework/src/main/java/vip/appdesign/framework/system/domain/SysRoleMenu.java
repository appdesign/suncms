package vip.appdesign.framework.system.domain;

import java.io.Serializable;

import com.baomidou.mybatisplus.annotation.TableId;
import lombok.Data;

/**
 * 角色
 * @TableName sys_role_menu
 */
@Data
public class SysRoleMenu implements Serializable {
    /**
     * 
     */
    @TableId
    private Long id;

    /**
     * 角色ID
     */
    private Long roleId;

    /**
     * 菜单ID
     */
    private Long menuId;

    private static final long serialVersionUID = 1L;
}