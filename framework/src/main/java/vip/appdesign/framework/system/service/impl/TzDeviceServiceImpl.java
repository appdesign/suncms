package vip.appdesign.framework.system.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import vip.appdesign.framework.system.domain.TzDevice;
import vip.appdesign.framework.system.service.TzDeviceService;
import vip.appdesign.framework.system.mapper.TzDeviceMapper;
import org.springframework.stereotype.Service;

/**
* @author sunpc
* @description 针对表【tz_device(台账设备坐标表)】的数据库操作Service实现
* @createDate 2024-07-17 14:49:46
*/
@Service
public class TzDeviceServiceImpl extends ServiceImpl<TzDeviceMapper, TzDevice>
    implements TzDeviceService{

}




