package vip.appdesign.framework.system.mapper;

import vip.appdesign.framework.system.domain.SysLog;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

import java.util.List;

/**
* @author sunpc
* @description 针对表【sys_log(系统日志表)】的数据库操作Mapper
* @createDate 2024-07-17 14:49:46
* @Entity vip.appdesign.framework.system.domain.SysLog
*/
public interface SysLogMapper extends BaseMapper<SysLog> {

    List<String> queryAllLog();
}




