package vip.appdesign.framework.config;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;


@Component
@ConfigurationProperties(prefix = "suncms")
public class SunCmsConfig {
    private  String name;
    private  String version;
    public static String upload;
    public static boolean redisOpen;

    public String getName() {
        return name;
    }
    public  void setName(String name) {
        this.name = name;
    }


    public  String getVersion() {
        return version;
    }

    public  void setVersion(String version) {
        this.version = version;
    }

    public static String getUpload() {
        return upload;
    }

    public  void setUpload(String upload) {
        SunCmsConfig.upload = upload;
    }
    public static boolean getRedisOpen() {
        return redisOpen;
    }
    public  void setRedisOpen(boolean redisOpen) {
        SunCmsConfig.redisOpen = redisOpen;
    }
}
