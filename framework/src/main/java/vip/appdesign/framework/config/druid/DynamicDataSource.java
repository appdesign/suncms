package vip.appdesign.framework.config.druid;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.jdbc.datasource.lookup.AbstractRoutingDataSource;
import vip.appdesign.common.enums.DataSourceType;

import javax.sql.DataSource;
import java.util.Map;


public class DynamicDataSource extends AbstractRoutingDataSource {
   private static final Logger log = LoggerFactory.getLogger(DynamicDataSource.class);
    //使用ThreadLocal维护变量，ThreadLocal为每个使用该变量的线程提供独立的变量副本，所以每一个线程都可以独立地改变自己的副本，而不会影响其它线程所对应的副本
    private static final ThreadLocal<String> dataSource = new InheritableThreadLocal<>();
    public DynamicDataSource(DataSource defaultTargetDataSource, Map<Object,Object> targetDataSource){
        super.setDefaultTargetDataSource(defaultTargetDataSource);
        super.setTargetDataSources(targetDataSource);
        super.afterPropertiesSet();
    }

    /**
     * 设置变量
     * @param type
     */
    public static void setDataSource(DataSourceType type) {
        log.info("切换到{}数据源",type.name());
        dataSource.set(type.name());
    }

    /**
     * 切换变量
     * @return
     */
    @Override
    protected Object determineCurrentLookupKey() {
        String dataSourceName = dataSource.get();
        if (dataSourceName == null) {
            dataSourceName = DataSourceType.MASTER.name();
        }
        log.info("获取{}数据源",dataSourceName);
        return dataSourceName;
    }

    /**
     * 清空变量
     */
    public static void clearDataSource() {
        dataSource.remove();
    }

}
