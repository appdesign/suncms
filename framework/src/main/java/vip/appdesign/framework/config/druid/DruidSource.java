package vip.appdesign.framework.config.druid;

import com.alibaba.druid.spring.boot.autoconfigure.DruidDataSourceBuilder;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;
import vip.appdesign.common.enums.DataSourceType;

import javax.sql.DataSource;
import java.util.HashMap;
import java.util.Map;

@Configuration
public class DruidSource {


    @ConfigurationProperties("spring.datasource.master")
    @Bean("masterDataSource")
    public DataSource masterDataSource(){
        return DruidDataSourceBuilder.create().build();
    }

    @ConfigurationProperties("spring.datasource.salve")
    @Bean("salveDataSource")
    public DataSource salveDataSource(){
        return DruidDataSourceBuilder.create().build();
    }
    @ConfigurationProperties("spring.datasource.cyjlerp")
    @Bean("cyjlerpDataSource")
    public DataSource cyjlerpDataSource(){
        return DruidDataSourceBuilder.create().build();
    }

    @Bean
    @Primary
    public DynamicDataSource dataSource(@Qualifier("masterDataSource") DataSource masterDataSource, @Qualifier("salveDataSource") DataSource salveDataSource,@Qualifier("cyjlerpDataSource") DataSource cyjlerpDataSource){
        Map<Object,Object> targetDataSource = new HashMap<>();
        targetDataSource.put(DataSourceType.MASTER.name(),masterDataSource);
        targetDataSource.put(DataSourceType.SALVE.name(),salveDataSource);
        targetDataSource.put(DataSourceType.CYJLERP.name(),cyjlerpDataSource);
        return  new DynamicDataSource(masterDataSource,targetDataSource);
    }

}