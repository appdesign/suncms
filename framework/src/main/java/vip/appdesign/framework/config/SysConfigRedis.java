package vip.appdesign.framework.config;

import jakarta.annotation.Resource;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import vip.appdesign.common.domain.Constant;
import vip.appdesign.common.redis.RedisUtils;
import vip.appdesign.common.utils.StringUtils;
import vip.appdesign.framework.system.domain.SysConfig;

/**
 * 系统配置Key
 */
@Component
public class SysConfigRedis {
    @Resource
    private RedisUtils redisUtils;

    /**
     * redis更新配置
     * @param sysConfig
     */
    public void saveOrUpdate(SysConfig sysConfig){
        if(sysConfig == null){
            return ;
        }
        redisUtils.set(Constant.REDIS_SYS_CONFIG_KEY+sysConfig.getConfigKey(),sysConfig);
    }

    /**
     * redis获取key
     * @param configKey
     * @return
     */
    public SysConfig get(String configKey){
        return redisUtils.get(Constant.REDIS_SYS_CONFIG_KEY+configKey,SysConfig.class);
    }

    /**
     * redis删除key
     * @param configKey
     */
    public void delete(String configKey){
        redisUtils.delete(configKey);
    }

}
