package vip.appdesign.framework.config;

import io.swagger.v3.oas.models.ExternalDocumentation;
import io.swagger.v3.oas.models.OpenAPI;
import io.swagger.v3.oas.models.info.Contact;
import io.swagger.v3.oas.models.info.Info;
import io.swagger.v3.oas.models.info.License;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;
import org.springframework.context.annotation.PropertySource;

@Configuration
@Primary
@PropertySource(value="classpath:springdoc.properties",encoding = "UTF-8")
public class OpenApiConfig {
    @Value("${termsOfService}")
    private String termsOfService;

    @Value("${title}")
    private String title;

    @Value("${description}")
    private String description;

    @Value("${version}")
    private String version;

    @Value("${contact.name}")
    private String contactName;

    @Value("${contact.url}")
    private String contactUrl;

    @Value("${contact.email}")
    private String contactEmail;

    @Value("${license.name}")
    private String licenseName;

    @Value("${license.url}")
    private String licenseUrl;

    @Value("${external.url}")
    private String externalUrl;

    @Value("${external.description}")
    private String externalDescription;

    @Value("${group.dev.name}")
    private String devGroupName;

    @Value("${group.test.name}")
    private String testGroupName;

    @Value("${group.proc.name}")
    private String procGroupName;

    @Value("${group.dev.path}")
    private String devGroupPath;

    @Value("${group.test.path}")
    private String testGroupPath;

    @Value("${group.proc.path}")
    private String procGroupPath;

    @Bean
    public OpenAPI openAPI() {
        return new OpenAPI()
        .info(new Info()
        .title(title)
        .description(description)
        .version(version)
        .contact(new Contact()
        .name(contactName)
        .url(contactUrl)
        .email(contactEmail))
        .termsOfService(termsOfService)
        .license(new License()
        .name(licenseName)
        .url(licenseUrl)))
        .externalDocs(new ExternalDocumentation()
        .description(externalDescription)
        .url(externalUrl));
    }
}
