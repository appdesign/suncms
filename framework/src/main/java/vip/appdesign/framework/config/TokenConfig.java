package vip.appdesign.framework.config;

import cn.dev33.satoken.config.SaTokenConfig;
import cn.dev33.satoken.exception.NotLoginException;
import cn.dev33.satoken.interceptor.SaInterceptor;
import cn.dev33.satoken.stp.StpUtil;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

import java.util.ArrayList;
import java.util.List;

@Configuration
public class TokenConfig implements WebMvcConfigurer {
    @Bean
    @Primary
    public SaTokenConfig saTokenConfig() {
        SaTokenConfig saTokenConfig = new SaTokenConfig();
        saTokenConfig.setTokenName("satoken");
        saTokenConfig.setTimeout(1*24*60*60);//Token失效时间;单位秒，默认1天
        saTokenConfig.setActiveTimeout(-1);// token 最低活跃频率（单位：秒），如果 token 超过此时间没有访问系统就会被冻结，默认-1 代表不限制，永不冻结
        saTokenConfig.setIsConcurrent(true);//是否允许同一账号多地同时登录 （为 true 时允许一起登录, 为 false 时新登录挤掉旧登录）
        saTokenConfig.setIsShare(true);//多人登录同一账号时，是否共用一个 token （为 true 时所有登录共用一个 token, 为 false 时每次登录新建一个 token）
        saTokenConfig.setTokenStyle("uuid");//token 风格（默认可取值：uuid、simple-uuid、random-32、random-64、random-128、tik）
        saTokenConfig.setIsLog(true);//是否输出日志
        return saTokenConfig;
    }

    @Override
    public void addInterceptors(InterceptorRegistry registry) {
        try {
            //注册Sa-Token拦截器,校验规则是StpUtil.checkLogin()登录校验
            List<String> list = new ArrayList<String>();
            list.add("/sys/login");
            list.add("/doc.html");
//            registry.addInterceptor(new SaInterceptor(handle-> StpUtil.checkLogin())).addPathPatterns("/sys/**").excludePathPatterns(list);
        }catch (NotLoginException e){

        }
    }
}
