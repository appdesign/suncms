package vip.appdesign.framework.common.api;


import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;

import java.io.Serializable;

/**
 * 通用响应返回类
 */
@Data
public class RS<T> implements Serializable {
    private static final long serialVersionUID = 1L;
    @Schema(description = "响应状态码",example = "200")
    private int code;
    @Schema(description = "响应消息")
    private String msg;
    @Schema(description = "响应数据")
    private T data;
    protected RS(int code , String msg, T data) {
        this.code = code;
        this.msg = msg;
        this.data = data;
    }

    /**
     * 成功返回
     * @return
     * @param <T>
     */
    public static <T> RS<T> success() {
        return new RS<>(RSCode.SUCCESS.getCode(), RSCode.SUCCESS.getMessage(), null);
    }

    /**
     * 成功返回
     * @param msg 提示信息
     * @return
     * @param <T>
     */
    public static <T> RS<T> success(String msg) {
        return new RS<>(RSCode.SUCCESS.getCode(), msg,null);
    }

    /**
     * 成功返回
     * @param data 获取的数据
     * @return
     * @param <T>
     */
    public static <T> RS<T> success(T data) {
        return new RS<>(RSCode.SUCCESS.getCode(),RSCode.SUCCESS.getMessage(), data);
    }

    /**
     * 成功返回
     * @param msg 提示信息
     * @param data 获取的数据
     * @return
     * @param <T>
     */
    public static <T> RS<T> success(String msg, T data) {
        return new RS<>(RSCode.SUCCESS.getCode(), msg,data);
    }

    /**
     * 失败返回结果
     * @return
     * @param <T>
     */
    public static <T> RS<T> failed() {
        return new RS<>(RSCode.FAILED.getCode(), RSCode.FAILED.getMessage(), null);
    }

    /**
     * 失败返回结果
     * @param msg 提示信息
     * @return
     * @param <T>
     */
    public static <T> RS<T> failed(String msg) {
        return new RS<>(RSCode.FAILED.getCode(), msg, null);
    }

    /**
     * 失败返回结果
     * @param code 状态码
     * @param msg 提示信息
     * @return
     * @param <T>
     */
    public static <T> RS<T> failed(int code, String msg) {
        return new RS<>(code,msg,null);
    }

}
