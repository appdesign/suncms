package vip.appdesign.framework.common.api;

/**
 * 封装API错误代码
 */
public interface IErrroCode {
    int getCode();
    String getMessage();
}
