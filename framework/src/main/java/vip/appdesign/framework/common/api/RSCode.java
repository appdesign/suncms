package vip.appdesign.framework.common.api;

/**
 * 枚举API操作
 */
public enum RSCode implements IErrroCode{
    SUCCESS(200,"操作成功"),
    FAILED(500,"操作失败"),
    VALIDATE_FAILED(400,"验证参数失败"),
    UNAUTHORIZED(401,"暂未登录或Token已经过期"),
    FORBIDDEN(403,"未授权")
    ;

    private final int code;
    private final String msg;
    RSCode(int code, String msg) {
        this.code = code;
        this.msg = msg;
    }
    @Override
    public int getCode() {
        return code;
    }

    @Override
    public String getMessage() {
        return msg;
    }
}
