package vip.appdesign.framework.oss.cloud;

/**
 * 云存储抽象类
 */
public abstract class CloudStorageService {
    /**
     * 云存储配置信息
     */
    CloudStorageConfig cloudStorageConfig;

    /**
     * 文件上传
     * @param bytes 文件字节数组
     * @param path 文件路径（包含文件）
     * @return HTTP文件地址
     */
    public abstract String upload(byte[] bytes,String path);

}
