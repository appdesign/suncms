package vip.appdesign.framework.oss.cloud;

import vip.appdesign.common.domain.Constant;
import vip.appdesign.common.utils.SpringBeanUtils;
import vip.appdesign.framework.system.service.SysConfigService;

/**
 * 云存储工厂类
 */

public class OSSFactory {
    private static SysConfigService sysConfigService;
    static {
        OSSFactory.sysConfigService= (SysConfigService) SpringBeanUtils.getBean("sysConfigService");
    }

    /**
     * 构造云存储服务
     * @return
     */
    public static CloudStorageService build(){
        //获取云存储配置
        CloudStorageConfig config = sysConfigService.getConfigObject(Constant.CLOUD_STORAGE_CONFIG_KEY, CloudStorageConfig.class);
        if(config.getType() == Constant.CloudService.TENCENT.getValue()){
            return new TencentCloudStorageService(config);
        }
        return null;
    }
}
