package vip.appdesign.framework.oss.cloud;

import com.qcloud.cos.COSClient;
import com.qcloud.cos.ClientConfig;
import com.qcloud.cos.auth.BasicCOSCredentials;
import com.qcloud.cos.auth.COSCredentials;
import com.qcloud.cos.http.HttpProtocol;
import com.qcloud.cos.model.ObjectMetadata;
import com.qcloud.cos.model.PutObjectResult;
import com.qcloud.cos.region.Region;
import vip.appdesign.common.exception.SunException;

import java.io.ByteArrayInputStream;
import java.io.InputStream;

/**
 * 腾讯云
 */
public class TencentCloudStorageService extends CloudStorageService{
    private COSClient cosClient;

    public TencentCloudStorageService(CloudStorageConfig cloudStorageConfig){
        this.cloudStorageConfig = cloudStorageConfig;
        init();
    }
    private void init(){

        try{
            // 1 初始化用户身份信息（secretId, secretKey）。
            // SECRETID 和 SECRETKEY 请登录访问管理控制台 https://console.cloud.tencent.com/cam/capi 进行查看和管理
            // 用户的 SecretId，建议使用子账号密钥，授权遵循最小权限指引，降低使用风险。子账号密钥获取可参见 https://cloud.tencent.com/document/product/598/37140
            String tencentSecretId = cloudStorageConfig.getTencentSecretId();
            String tencentSecretKey = cloudStorageConfig.getTencentSecretKey();
            COSCredentials cred = new BasicCOSCredentials(tencentSecretId, tencentSecretKey);
            // 2 设置 bucket 的地域, COS 地域的简称请参见 https://cloud.tencent.com/document/product/436/6224
            // clientConfig 中包含了设置 region, https(默认 http), 超时, 代理等 set 方法, 使用可参见源码或者常见问题 Java SDK 部分。
            Region region = new Region("ap-chengdu");
            ClientConfig clientConfig = new ClientConfig(region);
            // 这里建议设置使用 https 协议
            // 从 5.6.54 版本开始，默认使用了 https
            clientConfig.setHttpProtocol(HttpProtocol.https);
            // 3 生成 cos 客户端。
            COSClient cosClient = new COSClient(cred, clientConfig);
            this.cosClient = cosClient;
        }catch (Exception e){
            throw new SunException("TencentCloudStorageService init error", e);
        }


    }
    @Override
    public String upload(byte[] bytes, String path) {
        InputStream inputStream = new ByteArrayInputStream(bytes);
        String prefexPath = cloudStorageConfig.getTencentPrefix()+"/"+path;

        ObjectMetadata objectMetadata = new ObjectMetadata();
        objectMetadata.setContentLength(bytes.length);
        objectMetadata.setContentEncoding("UTF-8");
        try {
            PutObjectResult putObjectRequest = cosClient.putObject(cloudStorageConfig.getTencentBucketName(),prefexPath,inputStream,objectMetadata);
        }catch (Exception e){
            throw new SunException("TencentCloudStorageService upload error", e);
        }finally {
            if(cosClient!=null){
                cosClient.shutdown();
            }
        }
        return cloudStorageConfig.getTencentDomain()+"/"+prefexPath;
    }
}
