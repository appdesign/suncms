package vip.appdesign.framework.oss.cloud;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.multipart.MultipartFile;
import vip.appdesign.common.domain.Constant;
import vip.appdesign.common.utils.SpringBeanUtils;
import vip.appdesign.framework.file.FileUtils;
import vip.appdesign.framework.file.MIMEType;
import vip.appdesign.framework.system.service.SysConfigService;

/**
 * 云存储工具类
 */
public class OSSUtils {
    private static final Logger logger = LoggerFactory.getLogger(OSSUtils.class);
    private static SysConfigService sysConfigService;
    private static CloudStorageConfig cloudStorageConfig;
    private static int upload_type = 0;
    static {
        OSSUtils.sysConfigService = (SysConfigService) SpringBeanUtils.getBean("sysConfigService");
        cloudStorageConfig = sysConfigService.getConfigObject(Constant.CLOUD_STORAGE_CONFIG_KEY,CloudStorageConfig.class);
        upload_type = cloudStorageConfig.getType();
    }

    /**
     * 文件上传
     * @param file MultipartFile
     * @param module 模块文件
     * @return
     */
    public static String upload(MultipartFile file,String module) {
       return FileUtils.upload(upload_type,module,file, MIMEType.DEFAULT_ALLOWED_EXTENSION);
    }


}
