package vip.appdesign.framework.oss.cloud;

import lombok.Data;

import java.io.Serializable;

/**
 * 云存储配置信息
 */
@Data
public class CloudStorageConfig implements Serializable {
    private static final long serialVersionUID = 1L;
    //类型:0本地,1七牛云,2阿里云,3腾讯云
    private Integer type;
    //七牛云绑定域名
    private String qiniuDomain;
    //七牛云前缀
    private String qiniuPrefix;
    //七牛ACCESS_KEY
    private String qiniuAccessKey;
    //七牛SECRET_KEY
    private String qiniuSecretKey;
    //七牛云空间
    private String qiniuBucketName;

    //阿里云配置
    private String aliyunDomain;
    private String aliyunEndPoint;//指定区域
    private String aliyunPrefix;
    private String aliyunAccessKeyId;
    private String aliyunAccessKeySecret;
    private String aliyunBucketName;

    //腾讯云配置
    private String tencentDomain;
    private String tencentPrefix;
    private String tencentAppId;//5.6.155此版本初始化被弃用，推荐 new BasicCOSCredentials(secretId, secretKey);
    private String tencentSecretId;
    private String tencentSecretKey;
    private String tencentBucketName;

}
