package vip.appdesign.sun.modules.sys.service;

import vip.appdesign.sun.modules.sys.domain.TzPosition;
import com.baomidou.mybatisplus.extension.service.IService;

/**
* @author sun
* @description 针对表【tz_position(台账设备坐标表)】的数据库操作Service
* @createDate 2024-08-10 08:46:52
*/
public interface TzPositionService extends IService<TzPosition> {

}
