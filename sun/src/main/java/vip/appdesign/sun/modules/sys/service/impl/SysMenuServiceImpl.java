package vip.appdesign.sun.modules.sys.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import vip.appdesign.sun.modules.sys.domain.SysMenu;
import vip.appdesign.sun.modules.sys.service.SysMenuService;
import vip.appdesign.sun.modules.sys.mapper.SysMenuMapper;
import org.springframework.stereotype.Service;

/**
* @author sun
* @description 针对表【sys_menu(系统菜单管理)】的数据库操作Service实现
* @createDate 2024-08-10 08:46:52
*/
@Service
public class SysMenuServiceImpl extends ServiceImpl<SysMenuMapper, SysMenu>
    implements SysMenuService{

}




