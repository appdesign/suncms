package vip.appdesign.sun.modules.sys.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import vip.appdesign.sun.modules.sys.domain.TzPosition;
import vip.appdesign.sun.modules.sys.service.TzPositionService;
import vip.appdesign.sun.modules.sys.mapper.TzPositionMapper;
import org.springframework.stereotype.Service;

/**
* @author sun
* @description 针对表【tz_position(台账设备坐标表)】的数据库操作Service实现
* @createDate 2024-08-10 08:46:52
*/
@Service
public class TzPositionServiceImpl extends ServiceImpl<TzPositionMapper, TzPosition>
    implements TzPositionService{

}




