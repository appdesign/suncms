package vip.appdesign.sun.modules.sys.service;

import vip.appdesign.sun.modules.sys.domain.SysMenu;
import com.baomidou.mybatisplus.extension.service.IService;

/**
* @author sun
* @description 针对表【sys_menu(系统菜单管理)】的数据库操作Service
* @createDate 2024-08-10 08:46:52
*/
public interface SysMenuService extends IService<SysMenu> {

}
