package vip.appdesign.sun.modules.sys.service;

import com.baomidou.mybatisplus.extension.service.IService;
import vip.appdesign.sun.modules.sys.controller.domain.TrankKdysglVo;
import vip.appdesign.sun.modules.sys.domain.TrankKdysgl;

import java.util.List;

/**
* @author sun
* @description 针对表【trank_kdysgl】的数据库操作Service
* @createDate 2024-08-10 08:46:52
*/
public interface TrankKdysglService extends IService<TrankKdysgl> {
    void saveOrder(List<TrankKdysglVo> list) throws Exception;
}
