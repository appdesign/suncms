package vip.appdesign.sun.modules.sys.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import vip.appdesign.sun.modules.sys.domain.SysCaptcha;
import vip.appdesign.sun.modules.sys.service.SysCaptchaService;
import vip.appdesign.sun.modules.sys.mapper.SysCaptchaMapper;
import org.springframework.stereotype.Service;

/**
* @author sun
* @description 针对表【sys_captcha(系统验证码)】的数据库操作Service实现
* @createDate 2024-08-10 08:46:52
*/
@Service
public class SysCaptchaServiceImpl extends ServiceImpl<SysCaptchaMapper, SysCaptcha>
    implements SysCaptchaService{

}




