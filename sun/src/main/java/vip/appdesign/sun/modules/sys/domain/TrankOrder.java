package vip.appdesign.sun.modules.sys.domain;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import java.io.Serializable;
import lombok.Data;

/**
 * 
 * @TableName trank_order
 */
@TableName(value ="trank_order")
@Data
public class TrankOrder implements Serializable {
    /**
     * 快递单号
     */
    private String order;

    @TableField(exist = false)
    private static final long serialVersionUID = 1L;
}