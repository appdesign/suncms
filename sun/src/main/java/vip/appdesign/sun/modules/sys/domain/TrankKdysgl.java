package vip.appdesign.sun.modules.sys.domain;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import java.io.Serializable;
import lombok.Data;

/**
 * 
 * @TableName trank_kdysgl
 */
@TableName(value ="trank_kdysgl")
@Data
public class TrankKdysgl implements Serializable {
    /**
     * 货主原出库单号
     */
    private String hzyckdh;

    /**
     * 电商单号
     */
    private String dsdh;

    /**
     * 金额合计
     */
    private String jehj;

    /**
     * 总件数
     */
    private String zjs;

    /**
     * 制单人
     */
    private String zdr;

    /**
     * 原来源系统
     */
    private String ylyxt;

    /**
     * 单位主数据编码
     */
    private String khbm;

    @TableField(exist = false)
    private static final long serialVersionUID = 1L;
}