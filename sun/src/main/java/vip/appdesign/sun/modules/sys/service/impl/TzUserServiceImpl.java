package vip.appdesign.sun.modules.sys.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import vip.appdesign.sun.modules.sys.domain.TzUser;
import vip.appdesign.sun.modules.sys.service.TzUserService;
import vip.appdesign.sun.modules.sys.mapper.TzUserMapper;
import org.springframework.stereotype.Service;

/**
* @author sun
* @description 针对表【tz_user(台账用户表)】的数据库操作Service实现
* @createDate 2024-08-10 08:46:52
*/
@Service
public class TzUserServiceImpl extends ServiceImpl<TzUserMapper, TzUser>
    implements TzUserService{

}




