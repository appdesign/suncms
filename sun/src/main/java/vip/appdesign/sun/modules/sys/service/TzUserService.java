package vip.appdesign.sun.modules.sys.service;

import vip.appdesign.sun.modules.sys.domain.TzUser;
import com.baomidou.mybatisplus.extension.service.IService;

/**
* @author sun
* @description 针对表【tz_user(台账用户表)】的数据库操作Service
* @createDate 2024-08-10 08:46:52
*/
public interface TzUserService extends IService<TzUser> {

}
