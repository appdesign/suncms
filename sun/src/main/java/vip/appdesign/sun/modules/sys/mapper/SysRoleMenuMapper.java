package vip.appdesign.sun.modules.sys.mapper;

import vip.appdesign.sun.modules.sys.domain.SysRoleMenu;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
* @author sun
* @description 针对表【sys_role_menu(角色)】的数据库操作Mapper
* @createDate 2024-08-10 08:46:52
* @Entity vip.appdesign.sun.modules.sys.domain.SysRoleMenu
*/
public interface SysRoleMenuMapper extends BaseMapper<SysRoleMenu> {

}




