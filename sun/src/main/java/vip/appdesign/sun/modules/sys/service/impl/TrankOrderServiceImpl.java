package vip.appdesign.sun.modules.sys.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import vip.appdesign.sun.modules.sys.domain.TrankOrder;
import vip.appdesign.sun.modules.sys.service.TrankOrderService;
import vip.appdesign.sun.modules.sys.mapper.TrankOrderMapper;
import org.springframework.stereotype.Service;

/**
* @author sun
* @description 针对表【trank_order】的数据库操作Service实现
* @createDate 2024-08-10 08:46:52
*/
@Service
public class TrankOrderServiceImpl extends ServiceImpl<TrankOrderMapper, TrankOrder>
    implements TrankOrderService{

}




