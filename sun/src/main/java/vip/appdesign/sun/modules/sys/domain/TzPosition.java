package vip.appdesign.sun.modules.sys.domain;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import java.io.Serializable;
import lombok.Data;

/**
 * 台账设备坐标表
 * @TableName tz_position
 */
@TableName(value ="tz_position")
@Data
public class TzPosition implements Serializable {
    /**
     * 
     */
    @TableId(type = IdType.AUTO)
    private Long id;

    /**
     * 用户ID
     */
    private Long userId;

    /**
     * 设备ID
     */
    private Long deviceId;

    /**
     * 所属区域
     */
    private String areaName;

    /**
     * 坐标位置
     */
    private String areaPosition;

    @TableField(exist = false)
    private static final long serialVersionUID = 1L;
}