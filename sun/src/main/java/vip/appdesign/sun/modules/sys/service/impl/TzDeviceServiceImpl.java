package vip.appdesign.sun.modules.sys.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import vip.appdesign.sun.modules.sys.domain.TzDevice;
import vip.appdesign.sun.modules.sys.service.TzDeviceService;
import vip.appdesign.sun.modules.sys.mapper.TzDeviceMapper;
import org.springframework.stereotype.Service;

/**
* @author sun
* @description 针对表【tz_device(台账设备坐标表)】的数据库操作Service实现
* @createDate 2024-08-10 08:46:52
*/
@Service
public class TzDeviceServiceImpl extends ServiceImpl<TzDeviceMapper, TzDevice>
    implements TzDeviceService{

}




