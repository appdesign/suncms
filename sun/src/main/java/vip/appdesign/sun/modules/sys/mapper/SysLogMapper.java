package vip.appdesign.sun.modules.sys.mapper;

import vip.appdesign.sun.modules.sys.domain.SysLog;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
* @author sun
* @description 针对表【sys_log(系统日志表)】的数据库操作Mapper
* @createDate 2024-08-10 08:46:52
* @Entity vip.appdesign.sun.modules.sys.domain.SysLog
*/
public interface SysLogMapper extends BaseMapper<SysLog> {

}




