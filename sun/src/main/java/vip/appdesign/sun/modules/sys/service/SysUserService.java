package vip.appdesign.sun.modules.sys.service;

import vip.appdesign.sun.modules.sys.domain.SysUser;
import com.baomidou.mybatisplus.extension.service.IService;

/**
* @author sun
* @description 针对表【sys_user(系统用户)】的数据库操作Service
* @createDate 2024-08-10 08:46:52
*/
public interface SysUserService extends IService<SysUser> {

}
