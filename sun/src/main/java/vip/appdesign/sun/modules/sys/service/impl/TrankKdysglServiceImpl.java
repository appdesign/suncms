package vip.appdesign.sun.modules.sys.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.apache.ibatis.session.ExecutorType;
import org.apache.ibatis.session.SqlSession;
import org.apache.ibatis.session.SqlSessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import vip.appdesign.sun.modules.sys.controller.domain.TrankKdysglVo;
import vip.appdesign.sun.modules.sys.domain.TrankKdysgl;
import vip.appdesign.sun.modules.sys.service.TrankKdysglService;
import vip.appdesign.sun.modules.sys.mapper.TrankKdysglMapper;
import org.springframework.stereotype.Service;

import java.util.List;

/**
* @author sun
* @description 针对表【trank_kdysgl】的数据库操作Service实现
* @createDate 2024-08-10 08:46:52
*/
@Service
public class TrankKdysglServiceImpl extends ServiceImpl<TrankKdysglMapper, TrankKdysgl>
    implements TrankKdysglService{

    @Autowired
    private SqlSessionFactory sqlSessionFactory;

    @Override
    public void saveOrder(List<TrankKdysglVo> list) throws Exception {
        SqlSession sqlSession = sqlSessionFactory.openSession(ExecutorType.BATCH,false);
        TrankKdysglMapper mapper = sqlSession.getMapper(TrankKdysglMapper.class);
        try {
            for (TrankKdysglVo dysgl : list) {
                mapper.insertKdysgl(dysgl);
            }
            sqlSession.commit();
        }catch (Exception e){
            throw new Exception("导入异常");
        }finally {
            sqlSession.close();
        }
    }
}




