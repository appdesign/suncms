package vip.appdesign.sun.modules.sys.service;

import vip.appdesign.sun.modules.sys.domain.TrankOrder;
import com.baomidou.mybatisplus.extension.service.IService;

/**
* @author sun
* @description 针对表【trank_order】的数据库操作Service
* @createDate 2024-08-10 08:46:52
*/
public interface TrankOrderService extends IService<TrankOrder> {

}
