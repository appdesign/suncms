package vip.appdesign.sun.modules.sys.domain;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import java.io.Serializable;
import lombok.Data;

/**
 * 台账用户表
 * @TableName tz_user
 */
@TableName(value ="tz_user")
@Data
public class TzUser implements Serializable {
    /**
     * 
     */
    @TableId(type = IdType.AUTO)
    private Long id;

    /**
     * 用户ID
     */
    private Long userId;

    /**
     * 用名称
     */
    private String userName;

    /**
     * 归属部门
     */
    private String deptName;

    /**
     * 状态：0离职,1在职
     */
    private Integer status;

    @TableField(exist = false)
    private static final long serialVersionUID = 1L;
}