package vip.appdesign.sun.modules.sys.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import vip.appdesign.sun.modules.sys.domain.SysUserRole;
import vip.appdesign.sun.modules.sys.service.SysUserRoleService;
import vip.appdesign.sun.modules.sys.mapper.SysUserRoleMapper;
import org.springframework.stereotype.Service;

/**
* @author sun
* @description 针对表【sys_user_role(用户对应的角色表)】的数据库操作Service实现
* @createDate 2024-08-10 08:46:52
*/
@Service
public class SysUserRoleServiceImpl extends ServiceImpl<SysUserRoleMapper, SysUserRole>
    implements SysUserRoleService{

}




