package vip.appdesign.sun.modules.sys.controller.domain;

import com.alibaba.excel.annotation.ExcelProperty;
import lombok.Data;

@Data
public class ExcelVo {
    @ExcelProperty(value = "快递单号")
    private String order;
    @ExcelProperty(value = "总单号")
    private String xszdh;
}
