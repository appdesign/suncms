package vip.appdesign.sun.modules.sys.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;
import vip.appdesign.sun.common.poi.ExcelUtils;
import vip.appdesign.sun.modules.sys.controller.domain.ExcelVo;
import vip.appdesign.sun.modules.sys.controller.domain.TrankKdysglVo;
import vip.appdesign.sun.modules.sys.service.TrankKdysglService;

import java.io.IOException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.Statement;
import java.util.List;

/**
 * 工作辅助工具：物流单号匹配
 * @auther sun
 */
@RestController
@RequestMapping("/trankging")
public class TrankingController{
    @Autowired
    private TrankKdysglService trankKdysglService;
    @PostMapping("/read")
    public String read(@RequestParam("file") MultipartFile file) throws Exception {
        List<TrankKdysglVo> list = ExcelUtils.readExcel(file.getInputStream(), TrankKdysglVo.class);
        Connection com = DriverManager.getConnection("jdbc:mysql://app.appdesign.vip:3306/suncms", "suncms","suncms123");

        Class.forName("com.mysql.cj.jdbc.Driver");
        //通过此方法返回一个可执行的对象
        Statement statement = com.createStatement();
        String sql = "";
      for(TrankKdysglVo trankKdysglVo : list){
           sql="insert into trank_kdysgl values('"+trankKdysglVo.getHzyckdh()+"','"+trankKdysglVo.getDsdh()+"','"+trankKdysglVo.getJehj()+"','"+trankKdysglVo.getZjs()+"','"+trankKdysglVo.getZdr()+"','"+trankKdysglVo.getYlyxt()+"','"+trankKdysglVo.getKhbm()+"')";

//          executeUpdate执行sql增删改语句
          try {
              statement.executeUpdate(sql);
          }catch (Exception e) {
              System.out.println(sql);
          }

        }


        statement.close();
        return "ok";
    }
    @PostMapping("/readOrder")
    public String readOrder(@RequestParam("file") MultipartFile file) throws Exception {
        List<ExcelVo> list = ExcelUtils.readExcel(file.getInputStream(), ExcelVo.class);
        Connection com = DriverManager.getConnection("jdbc:mysql://app.appdesign.vip:3306/suncms", "suncms","suncms123");

        Class.forName("com.mysql.cj.jdbc.Driver");
        //通过此方法返回一个可执行的对象
        Statement statement = com.createStatement();
        String sql = "";
        for(ExcelVo excelVo : list){
            sql="insert into trank_order values('"+excelVo.getOrder()+"','"+excelVo.getXszdh()+"')";

//          executeUpdate执行sql增删改语句
            try {
                statement.executeUpdate(sql);
            }catch (Exception e) {
                System.out.println(sql);
            }

        }


        statement.close();
        return "ok";
    }



}