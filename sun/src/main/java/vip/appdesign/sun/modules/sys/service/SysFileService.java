package vip.appdesign.sun.modules.sys.service;

import vip.appdesign.sun.modules.sys.domain.SysFile;
import com.baomidou.mybatisplus.extension.service.IService;

/**
* @author sun
* @description 针对表【sys_file(文件存储地址表)】的数据库操作Service
* @createDate 2024-08-10 08:46:52
*/
public interface SysFileService extends IService<SysFile> {

}
