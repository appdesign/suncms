package vip.appdesign.sun.modules.sys.mapper;

import vip.appdesign.sun.modules.sys.domain.SysFile;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
* @author sun
* @description 针对表【sys_file(文件存储地址表)】的数据库操作Mapper
* @createDate 2024-08-10 08:46:52
* @Entity vip.appdesign.sun.modules.sys.domain.SysFile
*/
public interface SysFileMapper extends BaseMapper<SysFile> {

}




