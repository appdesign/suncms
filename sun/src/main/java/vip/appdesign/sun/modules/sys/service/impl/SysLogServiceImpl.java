package vip.appdesign.sun.modules.sys.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import vip.appdesign.sun.modules.sys.domain.SysLog;
import vip.appdesign.sun.modules.sys.service.SysLogService;
import vip.appdesign.sun.modules.sys.mapper.SysLogMapper;
import org.springframework.stereotype.Service;

/**
* @author sun
* @description 针对表【sys_log(系统日志表)】的数据库操作Service实现
* @createDate 2024-08-10 08:46:52
*/
@Service
public class SysLogServiceImpl extends ServiceImpl<SysLogMapper, SysLog>
    implements SysLogService{

}




