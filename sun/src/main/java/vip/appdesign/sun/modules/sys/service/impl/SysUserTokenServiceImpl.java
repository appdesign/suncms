package vip.appdesign.sun.modules.sys.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import vip.appdesign.sun.modules.sys.domain.SysUserToken;
import vip.appdesign.sun.modules.sys.service.SysUserTokenService;
import vip.appdesign.sun.modules.sys.mapper.SysUserTokenMapper;
import org.springframework.stereotype.Service;

/**
* @author sun
* @description 针对表【sys_user_token(Token表)】的数据库操作Service实现
* @createDate 2024-08-10 08:46:52
*/
@Service
public class SysUserTokenServiceImpl extends ServiceImpl<SysUserTokenMapper, SysUserToken>
    implements SysUserTokenService{

}




