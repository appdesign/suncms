package vip.appdesign.sun.modules.sys.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import vip.appdesign.sun.modules.sys.domain.SysRoleMenu;
import vip.appdesign.sun.modules.sys.service.SysRoleMenuService;
import vip.appdesign.sun.modules.sys.mapper.SysRoleMenuMapper;
import org.springframework.stereotype.Service;

/**
* @author sun
* @description 针对表【sys_role_menu(角色)】的数据库操作Service实现
* @createDate 2024-08-10 08:46:52
*/
@Service
public class SysRoleMenuServiceImpl extends ServiceImpl<SysRoleMenuMapper, SysRoleMenu>
    implements SysRoleMenuService{

}




