package vip.appdesign.sun.modules.sys.domain;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import java.io.Serializable;
import java.util.Date;
import lombok.Data;

/**
 * 台账设备坐标表
 * @TableName tz_device
 */
@TableName(value ="tz_device")
@Data
public class TzDevice implements Serializable {
    /**
     * 
     */
    @TableId(type = IdType.AUTO)
    private Long id;

    /**
     * 设备名称
     */
    private String deviceName;

    /**
     * 设备规格
     */
    private String deviceGg;

    /**
     * 生产厂家
     */
    private String deviceCj;

    /**
     * 批号
     */
    private String devicePh;

    /**
     * 序列号(出差编号)
     */
    private String deviceSn;

    /**
     * 入账日期
     */
    private Date deviceBuyTime;

    /**
     * 设备编号
     */
    private String deviceBh;

    /**
     * 资产编号
     */
    private String deviceZcbh;

    /**
     * 设备IP地址
     */
    private String deviceIp;

    /**
     * 状态：0闲置、1使用、4停用
     */
    private Integer status;

    @TableField(exist = false)
    private static final long serialVersionUID = 1L;
}