package vip.appdesign.sun.modules.sys.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import vip.appdesign.sun.modules.sys.domain.SysUser;
import vip.appdesign.sun.modules.sys.service.SysUserService;
import vip.appdesign.sun.modules.sys.mapper.SysUserMapper;
import org.springframework.stereotype.Service;

/**
* @author sun
* @description 针对表【sys_user(系统用户)】的数据库操作Service实现
* @createDate 2024-08-10 08:46:52
*/
@Service
public class SysUserServiceImpl extends ServiceImpl<SysUserMapper, SysUser>
    implements SysUserService{

}




