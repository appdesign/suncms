package vip.appdesign.sun.modules.sys.mapper;

import vip.appdesign.sun.modules.sys.domain.SysMenu;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
* @author sun
* @description 针对表【sys_menu(系统菜单管理)】的数据库操作Mapper
* @createDate 2024-08-10 08:46:52
* @Entity vip.appdesign.sun.modules.sys.domain.SysMenu
*/
public interface SysMenuMapper extends BaseMapper<SysMenu> {

}




