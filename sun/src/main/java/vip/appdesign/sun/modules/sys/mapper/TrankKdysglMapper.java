package vip.appdesign.sun.modules.sys.mapper;

import vip.appdesign.sun.modules.sys.controller.domain.TrankKdysglVo;
import vip.appdesign.sun.modules.sys.domain.TrankKdysgl;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

import java.util.List;

/**
* @author sun
* @description 针对表【trank_kdysgl】的数据库操作Mapper
* @createDate 2024-08-10 08:46:52
* @Entity vip.appdesign.sun.modules.sys.domain.TrankKdysgl
*/
public interface TrankKdysglMapper extends BaseMapper<TrankKdysgl> {


    void insertKdysgl(TrankKdysglVo trankKdysgl);
}




