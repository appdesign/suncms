package vip.appdesign.sun.common.poi;

import lombok.Data;

import java.io.Serializable;
import java.util.List;
import java.util.Map;
import java.util.UUID;

/**
 * 表单模型
 */
@Data
public class ExcelModel implements Serializable {
    private String fileName;
    private String[] headMap;
    private String[] fieldMap;
    private List<Map<String,Object>> datalist;
    public ExcelModel(){

    }
    public ExcelModel(List<Map<String,Object>> datalist){
        this.datalist = datalist;
        if(headMap == null ){
            headMap = new String[datalist.get(0).size()];
        }
        if(fieldMap == null ){
            fieldMap = new String[datalist.get(0).size()];
        }
        fileName = UUID.randomUUID().toString();
        //设置表头和字段
        if(datalist.size() > 0){
            Map<String,Object> map = datalist.get(0);
            int i = 0;
            for (Map.Entry<String, Object> m : map.entrySet()) {

                System.out.print(m.getKey() + "    ");
                this.headMap[i]=m.getKey();
                this.fieldMap[i]=m.getKey();
                i++;
//                System.out.println(m.getValue());
            }
        }
    }

}
