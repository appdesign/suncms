package vip.appdesign.sun.common.poi;

import com.alibaba.excel.EasyExcel;
import com.alibaba.excel.ExcelWriter;
import com.alibaba.excel.write.metadata.WriteSheet;
import jakarta.servlet.http.HttpServletResponse;
import org.springframework.web.bind.annotation.RequestBody;


import java.io.InputStream;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * Excel工具类
 */
public class ExcelUtils {


    public static void dynamicExport(HttpServletResponse response, LinkedHashMap<String,DynamicExcelData> fieldMap, List<Map<String,Object>> list,String sheetName) throws Exception {
        if(list.isEmpty()){
            throw new Exception("没有表单数据");
        }
        if(fieldMap.isEmpty()){
            throw new Exception("表单字段为空、请指定");
        }
        int size = list.size();
        List<List<String>> dataList = new ArrayList<>();
        for(int i=0;i<size;i++){
            dataList.add(new ArrayList<>());
        }
    }

    public static void dataWrite(@RequestBody ExcelModel excelModel, HttpServletResponse response) throws Exception {
        response.setContentType("application/vnd.openxmlformats-officedocument.spreadsheetml.sheet");
        response.setCharacterEncoding("utf-8");
        try {
            String fileName = URLEncoder.encode(excelModel.getFileName(),"UTF-8");
            response.setHeader("Content-disposition", "attachment;filename=" + fileName + ".xlsx");
            EasyExcel.write(response.getOutputStream()).head(setHead(excelModel.getHeadMap())).sheet(excelModel.getFileName()).doWrite(setData(excelModel.getDatalist(),excelModel.getFieldMap()));
            // 这里需要设置不关闭流
        } catch (Exception e) {
            response.reset();
            response.setContentType("application/json");
            response.setCharacterEncoding("utf-8");
            throw new Exception("下载失败",e);
        }
    }

    /**
     * 设置表格表头
     * @param headMap Stirng[] 指定需要导出的表头
     * @return
     */
    public static List<List<String>> setHead(String[] headMap){
        List<List<String>> list = new ArrayList<List<String>>();
        for (String head : headMap ){
            List<String> h = new ArrayList<String>();
            h.add(head);
            list.add(h);
        }
        return list;
    }

    /**
     * 设置表单数据
     * @param datalist List<Map<String,Object>>格式、方便使用Map集合的数据处理
     * @param fieldMap String[] 指定需要的导出字段的数组
     * @return
     */
    public static List<List<Object>> setData(List<Map<String,Object>> datalist,String[] fieldMap){
        List<List<Object>> lists = new ArrayList<List<Object>>();
        for(Map<String,Object> map : datalist){
            List<Object> list = new ArrayList<Object>();
            for (int i=0;i<fieldMap.length;i++){
                list.add(map.get(fieldMap[i]));
            }
            lists.add(list);
        }
        return lists;
    }

    /**
     * EasyExcel流的方式进行写入
     * @param datalist
     * @param filePath
     * @param sheetName
     */
    public static void writeExcel(List<List<Object>> datalist,String filePath,String sheetName){
        ExcelWriter easyWriter = EasyExcel.write(filePath).build();
        WriteSheet sheet = EasyExcel.writerSheet(sheetName).build();
        for (List<Object> data : datalist){
            List<String> row = data.stream().map(Object::toString).collect(Collectors.toList());
            easyWriter.write(row,sheet);
         }
        easyWriter.finish();
    }

    /**
     * 读取Excel转为list
     * @param inputStream
     * @param head
     * @return
     */
    public static <T> List<T> readExcel(InputStream inputStream,Class<T> head) {
//        ExcelListener listener = new ExcelListener();
        return EasyExcel.read(inputStream,head,null).autoCloseStream(false).doReadAllSync();
    }


}
