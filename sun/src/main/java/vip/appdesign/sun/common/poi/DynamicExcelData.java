package vip.appdesign.sun.common.poi;

import lombok.Data;

@Data
public class DynamicExcelData {
    private String fieldName;//字段名称
    private Object fieldValue;//字段值
    public DynamicExcelData(String fieldName, Object fieldValue) {
        this.fieldName = fieldName;
        this.fieldValue = fieldValue;
    }

}
