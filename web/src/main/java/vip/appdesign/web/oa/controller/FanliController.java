package vip.appdesign.web.oa.controller;

import jakarta.servlet.http.HttpServletResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import vip.appdesign.common.controller.BaseController;
import vip.appdesign.common.utils.http.HttpContextUtils;
import vip.appdesign.common.utils.poi.ExcelModel;
import vip.appdesign.common.utils.poi.ExcelUtils;
import vip.appdesign.web.oa.service.FanliService;

import java.util.Map;

@RestController
@RequestMapping("/oa/fanli")
public class FanliController extends BaseController {
    @Autowired
    private FanliService fanliService;
    @GetMapping("/tj")
    public void queryFanli(@RequestParam Map<String, Object> params) {
        ExcelModel model = new ExcelModel(fanliService.queryFanli(params));
        HttpServletResponse res = HttpContextUtils.getHttpServletResponse();
        ExcelUtils.dataWrite(model,res);
    }
}
