package vip.appdesign.web.oa.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Primary;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Service;
import vip.appdesign.common.annotation.SwitchData;
import vip.appdesign.common.enums.DataSourceType;
import vip.appdesign.web.oa.service.FanliService;

import java.util.List;
import java.util.Map;

@Service
public class FanliServiceImpl implements FanliService {
    @Autowired
    private JdbcTemplate jdbcTemplate;

    @SwitchData(DataSourceType.SALVE)
    @Override
    public List<Map<String, Object>> queryFanli(Map<String, Object> map) {
       return jdbcTemplate.queryForList("select a.code,a.sqr,a.sqrq,a.ksrq,a.jsrq,a.dwbh,a.danwbh,b.dwmch,b.lxr,b.lxrdh,b.quyufl,b.ywy,c.realyname,d.deptname,isnull(a.status,'0') status,is_check,is_money,is_sum,a.zcqx,a.zcbs,a.flxs from tblfanlihz_fj a left join mchk b on a.dwbh = b.dwbh left join tbluser c on a.sqr=c.code left join ralationinfo r on a.sqr = r.usercode left join tbldept d on r.deptcode = d.code where a.sqrq between '2003-01-01' and '2024-07-31' order by a.code desc");
    }



}
