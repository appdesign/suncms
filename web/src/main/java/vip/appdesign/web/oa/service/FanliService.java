package vip.appdesign.web.oa.service;

import java.util.List;
import java.util.Map;

/**
 * 返利服务
 */
public interface FanliService {
    List<Map<String,Object>> queryFanli(Map<String,Object> map);
}
