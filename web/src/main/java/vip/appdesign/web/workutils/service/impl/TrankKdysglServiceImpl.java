package vip.appdesign.web.workutils.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.apache.ibatis.session.ExecutorType;
import org.apache.ibatis.session.SqlSession;
import org.apache.ibatis.session.SqlSessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import vip.appdesign.common.exception.SunException;
import vip.appdesign.web.workutils.domain.ExcelVo;
import vip.appdesign.web.workutils.domain.TrankKdysgl;
import vip.appdesign.web.workutils.service.TrankKdysglService;
import vip.appdesign.web.workutils.mapper.TrankKdysglMapper;
import org.springframework.stereotype.Service;

import java.util.Collection;
import java.util.List;

/**
* @author sun
* @description 针对表【trank_kdysgl】的数据库操作Service实现
* @createDate 2024-08-09 22:00:31
*/
@Service
public class TrankKdysglServiceImpl extends ServiceImpl<TrankKdysglMapper, TrankKdysgl>
    implements TrankKdysglService{

    @Autowired
    private SqlSessionFactory sqlSessionFactory;


    @Override
    public void saveOrder(List<TrankKdysgl> list) {
        SqlSession sqlSession = sqlSessionFactory.openSession(ExecutorType.BATCH,false);
        TrankKdysglMapper mapper = sqlSession.getMapper(TrankKdysglMapper.class);
        try {
            for (TrankKdysgl trankKdysgl : list) {
                mapper.insertKdysgl(trankKdysgl);
            }
            sqlSession.commit();
        }catch (Exception e){
            throw new SunException("导入异常");
        }finally {
            sqlSession.close();
        }
    }

    @Override
    public void saveBatchs(List<TrankKdysgl> list) {
        baseMapper.saveBatchs(list);
    }
}




