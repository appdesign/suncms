package vip.appdesign.web.workutils.domain;

import com.alibaba.excel.annotation.ExcelProperty;
import lombok.Data;

@Data
public class ExcelVo {
    @ExcelProperty(value = "快递单号")
    private String order;
}
