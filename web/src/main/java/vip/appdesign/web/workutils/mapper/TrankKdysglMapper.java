package vip.appdesign.web.workutils.mapper;

import vip.appdesign.web.workutils.domain.TrankKdysgl;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

import java.util.List;

/**
* @author sun
* @description 针对表【trank_kdysgl】的数据库操作Mapper
* @createDate 2024-08-09 22:00:31
* @Entity vip.appdesign.web.workutils.domain.TrankKdysgl
*/
public interface TrankKdysglMapper extends BaseMapper<TrankKdysgl> {

    void insertKdysgl(TrankKdysgl trankKdysgl);

    void saveBatchs(List<TrankKdysgl> list);
}




