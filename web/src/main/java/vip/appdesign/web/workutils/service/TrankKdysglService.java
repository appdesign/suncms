package vip.appdesign.web.workutils.service;

import vip.appdesign.web.workutils.domain.ExcelVo;
import vip.appdesign.web.workutils.domain.TrankKdysgl;
import com.baomidou.mybatisplus.extension.service.IService;

import java.util.List;

/**
* @author sun
* @description 针对表【trank_kdysgl】的数据库操作Service
* @createDate 2024-08-09 22:00:31
*/
public interface TrankKdysglService extends IService<TrankKdysgl> {
    void saveOrder(List<TrankKdysgl> list);

    void saveBatchs(List<TrankKdysgl> list);
}
