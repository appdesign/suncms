package vip.appdesign.web.workutils.controller;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.tags.Tag;
import jakarta.servlet.http.HttpServletResponse;
import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import org.apache.poi.ss.usermodel.*;
import org.apache.poi.xssf.usermodel.XSSFCell;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;
import vip.appdesign.common.controller.BaseController;
import vip.appdesign.common.domain.R;
import vip.appdesign.common.exception.SunException;
import vip.appdesign.common.utils.poi.ExcelUtils;
import vip.appdesign.web.workutils.domain.TrankKdysgl;
import vip.appdesign.web.workutils.domain.TrankOrder;
import vip.appdesign.web.workutils.service.TrankKdysglService;
import vip.appdesign.web.workutils.service.TrankOrderService;

import java.io.*;
import java.util.List;

/**
 * 工作辅助工具：物流单号匹配
 * @auther sun
 */
@Tag(name = "快递匹配工具")
@RestController
@RequestMapping("/trankging")
public class TrankingController extends BaseController {
    @Autowired
    private TrankKdysglService trankKdysglService;
    @Autowired
    private TrankOrderService trankOrderService;

    @Operation(summary = "WMS快递运输管理上传")
    @PostMapping("/wmsUpload")
    public R wmsUpload(@RequestParam("file") MultipartFile file) throws IOException {
        //当前时间毫秒
        long start = System.currentTimeMillis();
        List<TrankKdysgl> list = ExcelUtils.readExcel(file.getInputStream(), TrankKdysgl.class);
        trankKdysglService.saveOrder(list);
        long end = System.currentTimeMillis();

        return R.success("耗时：", end - start+"毫秒！");
    }

    @Operation(summary = "SCM物流单号上传")
    @PostMapping( "/scmUpload")
    public R scmUpload(@RequestParam("file") MultipartFile file) throws IOException {
        //当前时间毫秒
        long start = System.currentTimeMillis();
        List<TrankOrder> list = ExcelUtils.readExcel(file.getInputStream(), TrankOrder.class);
        trankOrderService.saveBatch(list);
        long end = System.currentTimeMillis();
        return R.success("耗时：", end - start+"毫秒！");
    }

    @Operation(summary = "快递账单上传",description = "找到快递单号列右侧新增6列空白列")
    @PostMapping( "/kddhUpload")
    public R kddhUpload(@RequestParam("file") MultipartFile file){
        if (!file.isEmpty()) {
            try {
                //当前时间毫秒
                long start = System.currentTimeMillis();
                String file_path = file.getOriginalFilename();
                BufferedOutputStream out = new BufferedOutputStream(
                        new FileOutputStream(new File(file_path)));
                out.write(file.getBytes());
                out.flush();
                out.close();

                long end = System.currentTimeMillis();
                return R.success("耗时：", end - start+"毫秒！");
            } catch (IOException e) {
                logger.error("file upload fail: " + e.getMessage());
                return R.error(e.getMessage());
            }
        } else {
            logger.error("file upload fail!");
            return R.error("上传失败！！！");
        }
    }

    @Operation(summary = "订单匹配（先导入SCM物流单号、WMS快递单号、快递提供的每月账单号）")
    @PostMapping("/kddhMatch")
    public void kddhMatch(@RequestParam("file") String file, HttpServletResponse response) throws IOException, InvalidFormatException {
        //当前时间毫秒
        long start = System.currentTimeMillis();
        Workbook workbook = new XSSFWorkbook(new File(file));
        Sheet sheet = workbook.getSheetAt(0);
        int orderIndex = -1;
        System.out.println("匹配总行数："+sheet.getLastRowNum());
        List<TrankKdysgl> listKdysgl = trankKdysglService.list();
        List<TrankOrder> listOrder = trankOrderService.list();
        for (int i = 0; i < sheet.getLastRowNum(); i++) {
            Row row = sheet.getRow(i);
            if (i == 0) {
                for (int j = 0; j < row.getLastCellNum(); j++) {
                    Cell cell = row.getCell(i);
                    String value = ExcelUtils.getCellValue(cell);

                    if (value.equals("单号")) {
                        orderIndex = j;
                        sheet.getRow(i).getCell(orderIndex+1).setCellValue("销售总单号");
                        sheet.getRow(i).getCell(orderIndex+2).setCellValue("电商单号");
                        sheet.getRow(i).getCell(orderIndex+3).setCellValue("订单类型");
                        sheet.getRow(i).getCell(orderIndex+4).setCellValue("总金额");
                        sheet.getRow(i).getCell(orderIndex+5).setCellValue("件数");
                        sheet.getRow(i).getCell(orderIndex+6).setCellValue("制单人");
                        break;
                    }
                }

                if (orderIndex == -1) {
                    throw new SunException("没有匹配到《单号》列");
                }

            } else {
                    Cell cellOrder= sheet.getRow(i).getCell(orderIndex);
                    if(cellOrder != null){

                              for(TrankOrder trankOrder : listOrder){
                                    //获取快递单号对应的出库单号
                                    if(trankOrder.getKddh().equals(cellOrder.getStringCellValue())){
                                        for(TrankKdysgl trankKdysgl : listKdysgl){
                                            //快递运输管理总单号包含物流单号

                                            if(trankKdysgl.getHzyckdh() ==null){
                                                continue;
                                            }
                                            if(trankKdysgl.getHzyckdh().contains(trankOrder.getHzyckdh())){
                                                System.out.println("正在匹配写入行数据:"+i);

                                                sheet.getRow(i).getCell(orderIndex+1).setCellValue(trankKdysgl.getHzyckdh());
                                                sheet.getRow(i).getCell(orderIndex+2).setCellValue(trankKdysgl.getDsdh());
                                                sheet.getRow(i).getCell(orderIndex+3).setCellValue(trankKdysgl.getYlyxt());
                                                sheet.getRow(i).getCell(orderIndex+4).setCellValue(trankKdysgl.getZjs());
                                                sheet.getRow(i).getCell(orderIndex+5).setCellValue(trankKdysgl.getJehj());
                                                sheet.getRow(i).getCell(orderIndex+6).setCellValue(trankKdysgl.getZdr());
                                            }
                                        }
                                    }

                                  }
                              }

                    }


            }
        response.setHeader("Content-Disposition", "attachment; filename=download.xlsx");
        response.setContentType("application/vnd.openxmlformats-officedocument.spreadsheetml.sheet");
        // 写入响应流
        OutputStream outputStream = response.getOutputStream();
        workbook.write(outputStream);
        workbook.close();
        outputStream.close();
        long end = System.currentTimeMillis();

    }

    @Operation(summary = "导出excel转list测试")
    @PostMapping("/test")
    public R test(@RequestParam("file") MultipartFile file) throws IOException {
        List<TrankKdysgl> list =  ExcelUtils.readExcelToList(file.getInputStream(),TrankKdysgl.class);
        return R.success(list);
    }

    @Operation(summary = "导出excel")
    @PostMapping("/testimport")
    public R testimport(@RequestParam("file") MultipartFile file) throws IOException {
        long start = System.currentTimeMillis();
        List<TrankKdysgl> list =  ExcelUtils.readExcelToList(file.getInputStream(),TrankKdysgl.class);
        long end = System.currentTimeMillis();
        System.out.println("加载内存："+(end-start)+"毫秒");
        trankKdysglService.saveBatchs(list);

        trankKdysglService.saveBatchs(list);
        long end2 = System.currentTimeMillis();
        System.out.println("加载写入数控："+(end2-end)+"毫秒");
        return R.success("写入成功");
    }

}
