package vip.appdesign.web.workutils.service;

import vip.appdesign.web.workutils.domain.TrankOrder;
import com.baomidou.mybatisplus.extension.service.IService;

/**
* @author sun
* @description 针对表【trank_order】的数据库操作Service
* @createDate 2024-08-10 13:14:14
*/
public interface TrankOrderService extends IService<TrankOrder> {

}
