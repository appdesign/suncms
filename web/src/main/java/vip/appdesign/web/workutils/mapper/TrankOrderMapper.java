package vip.appdesign.web.workutils.mapper;

import vip.appdesign.web.workutils.domain.TrankOrder;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
* @author sun
* @description 针对表【trank_order】的数据库操作Mapper
* @createDate 2024-08-10 13:14:14
* @Entity vip.appdesign.web.workutils.domain.TrankOrder
*/
public interface TrankOrderMapper extends BaseMapper<TrankOrder> {

}




