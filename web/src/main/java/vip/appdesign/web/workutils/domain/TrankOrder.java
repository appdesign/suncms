package vip.appdesign.web.workutils.domain;

import com.alibaba.excel.annotation.ExcelProperty;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import java.io.Serializable;
import lombok.Data;

/**
 * 
 * @TableName trank_order
 */
@TableName(value ="trank_order")
@Data
public class TrankOrder implements Serializable {
    /**
     * 快递单号
     */
    @ExcelProperty("快递单号")
    private String kddh;

    /**
     * 货主原出库单号
     */
    @ExcelProperty("货主原出库单号")
    private String hzyckdh;

    private static final long serialVersionUID = 1L;
}