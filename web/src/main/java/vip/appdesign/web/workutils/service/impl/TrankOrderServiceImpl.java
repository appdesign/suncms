package vip.appdesign.web.workutils.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import vip.appdesign.web.workutils.domain.TrankOrder;
import vip.appdesign.web.workutils.service.TrankOrderService;
import vip.appdesign.web.workutils.mapper.TrankOrderMapper;
import org.springframework.stereotype.Service;

/**
* @author sun
* @description 针对表【trank_order】的数据库操作Service实现
* @createDate 2024-08-10 13:14:14
*/
@Service
public class TrankOrderServiceImpl extends ServiceImpl<TrankOrderMapper, TrankOrder>
    implements TrankOrderService{

}




