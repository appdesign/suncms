package vip.appdesign.web.workutils.domain;

import java.io.Serializable;

import com.alibaba.excel.annotation.ExcelProperty;
import lombok.Data;

/**
 * 
 * @TableName trank_kdysgl
 */
@Data
public class TrankKdysgl implements Serializable {
    /**
     * 货主原出库单号
     */
    @ExcelProperty("货主原出库单号")
    private String hzyckdh;

    /**
     * 电商单号
     */
    @ExcelProperty("电商单号")
    private String dsdh;

    /**
     * 金额合计
     */
    @ExcelProperty("金额合计")
    private String jehj;

    /**
     * 总件数
     */
    @ExcelProperty("总件数")
    private String zjs;

    /**
     * 制单人
     */
    @ExcelProperty("制单人")
    private String zdr;

    /**
     * 原来源系统
     */
    @ExcelProperty("原单来源系统")
    private String ylyxt;

    /**
     * 单位主数据编码
     */
    @ExcelProperty("单位主数据编码")
    private String khbm;

    private static final long serialVersionUID = 1L;
}