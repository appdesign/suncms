package vip.appdesign.web.system;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.tags.Tag;
import jakarta.servlet.http.HttpServletResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import vip.appdesign.common.annotation.Log;
import vip.appdesign.common.controller.BaseController;
import vip.appdesign.common.domain.R;
import vip.appdesign.common.utils.PageUtils;
import vip.appdesign.common.utils.http.HttpContextUtils;
import vip.appdesign.common.utils.poi.ExcelModel;
import vip.appdesign.common.utils.poi.ExcelUtils;
import vip.appdesign.framework.system.service.SysLogService;

import java.util.List;
import java.util.Map;

/**
 * 系统日志
 */
@Tag(name = "系统日志")
@RestController
@RequestMapping("/sys/log")
public class SysLogController extends BaseController {
    @Autowired
    private SysLogService sysLogService;

    @Operation(summary = "日志列表")
    @Log("查询日志2024-07-22")
    @GetMapping("/list")
    public R list(@RequestParam Map<String,Object> params){
        PageUtils pageUtils = sysLogService.queryPage(params);
        return R.success().put("page",pageUtils);
    }
    @Operation(summary = "导出日志")
    @Log("导出日志")
    @GetMapping("/export")
    public void export(@RequestParam Map<String,Object> params){
        HttpServletResponse res = HttpContextUtils.getHttpServletResponse();
        List<Map<String,Object>> list = sysLogService.queryAllLog(params);
        ExcelModel model = new ExcelModel();
        model.setFileName("导出测试");
        String[] headMap ={"用户名","操作方式"};//自定义表头
        String[] dataMap ={"username","operation"};//自定义导出字符串
        model.setHeadMap(headMap);
        model.setFieldMap(dataMap);
        model.setDatalist(list);
        ExcelUtils.dataWrite(model,res);
    }
}
