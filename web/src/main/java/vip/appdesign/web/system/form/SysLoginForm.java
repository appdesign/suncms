package vip.appdesign.web.system.form;

import lombok.Data;

/**
 * 登录表单
 */
@Data
public class SysLoginForm {
    private String username;
    private String password;
    private String uuid;
    private String code;
}
