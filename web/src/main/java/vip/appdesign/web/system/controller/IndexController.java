package vip.appdesign.web.system.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import vip.appdesign.common.controller.BaseController;
import vip.appdesign.common.domain.R;
import vip.appdesign.framework.system.service.SysConfigService;

@RestController
@RequestMapping("/")
public class IndexController extends BaseController {
    @Autowired
    private SysConfigService sysConfigService;
    @GetMapping
    public R index() {

        return R.success("欢迎访问Web").put("config",sysConfigService.list());
    }
}
