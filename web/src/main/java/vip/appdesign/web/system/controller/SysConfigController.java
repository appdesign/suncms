package vip.appdesign.web.system.controller;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.tags.Tag;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import vip.appdesign.common.controller.BaseController;
import vip.appdesign.common.domain.R;
import vip.appdesign.common.utils.PageUtils;
import vip.appdesign.framework.common.api.RS;
import vip.appdesign.framework.config.SunCmsConfig;
import vip.appdesign.framework.system.domain.SysConfig;
import vip.appdesign.framework.system.service.SysConfigService;

import java.util.Map;

/**
 * 系统配置
 */
@Tag(name = "系统配置")
@RestController
@RequestMapping("/sys/config")
public class SysConfigController extends BaseController {
    @Autowired
    private SysConfigService sysConfigService;
    @Operation(summary = "获取配置列表")
    @GetMapping("/list")
    public R list(@RequestParam Map<String, Object> params) {
        PageUtils page = sysConfigService.queryPage(params);
        return R.success(page);
    }
    @Operation(summary = "根据ID获取配置信息")
    @GetMapping("/info/{id}")
    public R info(@PathVariable("id") Long id) {
        SysConfig sysConfig = sysConfigService.getById(id);
        return R.success(sysConfig);
    }
    @Operation(summary = "根据KEY获取配置信息")
    @GetMapping("/key/{key}")
    public R key(@PathVariable("key") String key) {
        String value= sysConfigService.getValue(key);
        return R.success("获取成功",value);
    }

}
