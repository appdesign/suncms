package vip.appdesign.web.system.controller;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.tags.Tag;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;
import vip.appdesign.common.domain.R;
import vip.appdesign.framework.oss.cloud.OSSUtils;

/**
 * 云存储控制类
 */
@Tag(name = "云存储管理")
@RestController
@RequestMapping("/sys/oss")
public class OSSController {
    @Operation(summary = "云存储上传")
    @PostMapping("/upload")
    public R upload(@RequestParam("file") MultipartFile file) {
        String ossPath = "";
        if(file.isEmpty()) {
            return R.error("上传文件为空");
        }
        ossPath= OSSUtils.upload(file,"suntest666");
        return R.success("文件上传成功").put("path", ossPath);
    }
}
