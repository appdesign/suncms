package vip.appdesign.web.system.controller;


import cn.dev33.satoken.secure.SaSecureUtil;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.tags.Tag;
import jakarta.servlet.ServletOutputStream;
import jakarta.servlet.http.HttpServletResponse;
import org.apache.commons.io.IOUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import vip.appdesign.common.domain.R;
import vip.appdesign.framework.system.domain.SysUser;
import vip.appdesign.framework.system.service.SysCaptchaService;
import vip.appdesign.framework.system.service.SysUserService;
import vip.appdesign.web.system.form.SysLoginForm;


import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.io.IOException;

@Tag(name = "登录")
@RestController
public class LoginController {
    @Autowired
    private SysCaptchaService sysCaptchaService;
    @Autowired
    private SysUserService sysUserService;
    @Operation(summary = "验证码获取")
    @GetMapping("captcha.jpg")
    public void captcha(HttpServletResponse response, @RequestParam(name = "uuid") String uuid) throws IOException {
        response.setHeader("Cache-Control", "no-store, no-cache");
        response.setContentType("image/jpg");
        //获取验证码图片
        BufferedImage image = sysCaptchaService.getCaptcha(uuid);
        ServletOutputStream out = response.getOutputStream();
        ImageIO.write(image,"jpg",out);
        IOUtils.closeQuietly(out);
    }
    /**
     * 登录
     * @param form
     * @return
     */
    @Operation(summary = "登录")
    @PostMapping("/sys/login")
    public R login(@RequestBody SysLoginForm form){
        boolean captch = sysCaptchaService.checkCaptcha(form.getUuid(), form.getCode());
        if(!captch){
            return R.error("验证码不正确");
        }
        //用户信息
        SysUser user = sysUserService.queryByUsername(form.getUsername());
        if(user == null || !user.getPassword().equals(SaSecureUtil.sha256(form.getPassword()+user.getSalt()))){
            return R.error("账号密码不正确");
        }
        if(user.getStatus() == 0){
            return R.error("账号已被锁定、请联系管理员");
        }
        return R.success("登录成功");
    }

}
